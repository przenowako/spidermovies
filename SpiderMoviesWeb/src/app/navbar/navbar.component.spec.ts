import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [
        HttpClientTestingModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
      ],
      providers: [{ provide: MatDialog, useValue: {} }],
    }).compileComponents();

    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create roles', () => {
    expect(component.roles).toEqual([]);
  });

  it('should create isAdmin', () => {
    expect(component.isAdmin).toBeFalsy();
  });

  it('should create isManager', () => {
    expect(component.isManager).toBeFalsy();
  });

  it('should create isLoggedIn', () => {
    expect(component.isLoggedIn).toBeFalsy();
  });

  it('should create isUser', () => {
    expect(component.isUser).toBeFalsy();
  });
});
