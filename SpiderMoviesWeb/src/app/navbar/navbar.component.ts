import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { EventBusService } from '../shared/event-bus.service';
import { MatDialog } from '@angular/material/dialog';
import { MovieDialogComponent } from '../dialogs/movie-dialog/movie-dialog.component';
import { GenresDialogComponent } from '../dialogs/genres-dialog/genres-dialog.component';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  roles: string[] = [];
  isLoggedIn = false;
  isAdmin = false;
  isUser = false;
  isManager = false;
  username?: string;

  eventBusSub?: Subscription;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private eventBusService: EventBusService,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.userService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.userService.getUserSession();
      this.roles = user.roles;

      this.isAdmin = this.roles.includes('ADMIN');
      this.isUser = this.roles.includes('USER');
      this.isManager = this.roles.includes('MANAGER');

      this.username = user.username;
    }

    this.eventBusSub = this.eventBusService.on('logout', () => {
      this.logout();
    });
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: (res) => {
        this.userService.clean();
        window.location.replace('/');
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  openMovieDialog(): void {
    this.dialog.open(MovieDialogComponent, {
      panelClass: 'dialog',
      data: { isNew: true },
    });
  }

  openGenreDialog(): void {
    this.dialog.open(GenresDialogComponent, {
      panelClass: 'dialog',
      data: { isNew: true },
    });
  }
}
