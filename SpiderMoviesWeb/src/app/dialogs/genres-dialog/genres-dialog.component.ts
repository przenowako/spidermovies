import { Component, Input, OnInit } from '@angular/core';
import { Genres } from '../../model/movie';
import { GenresService } from '../../services/genres.service';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-genres-dialog',
  templateUrl: './genres-dialog.component.html',
  styleUrls: ['./genres-dialog.component.scss'],
})
export class GenresDialogComponent implements OnInit {
  allGenres?: Genres[];
  selectedGenre?: Genres;
  newGenre?: Genres;
  newNameGenre?: string;
  updateNameGenre?: string;
  content?: string;
  isAdded: boolean = false;
  isDeleted: boolean = false;
  isUpdated: boolean = false;
  isSaveGenre: boolean = true;

  constructor(
    private movieService: MovieService,
    private genreService: GenresService
  ) {}

  ngOnInit(): void {
    this.getGenres();
  }

  getGenres(): void {
    this.movieService.getGenres().subscribe({
      next: (data) => {
        this.allGenres = data;
        this.isSaveGenre = true;
      },
      error: (err) => {
        this.content = err;
      },
    });
  }

  deSelectGenre(): void {
    this.selectedGenre = undefined;
  }

  updateGenre(): void {
    if (this.updateNameGenre && this.isSafeString(this.updateNameGenre)) {
      this.selectedGenre!.name = this.updateNameGenre;
      this.genreService.updateGenre(this.selectedGenre!).subscribe(() => {
        this.isUpdated = true;
        this.getGenres();
      });
      setTimeout(() => {
        this.isUpdated = false;
        this.deSelectGenre();
        this.updateNameGenre = '';
        this.isSaveGenre = true;
      }, 1000);
    } else {
      this.isSaveGenre = false;
    }
  }

  addGenre(): void {
    if (this.newNameGenre && this.isSafeString(this.newNameGenre)) {
      this.genreService.addGenre(this.newNameGenre).subscribe(() => {
        this.isAdded = true;
        this.getGenres();
      });
      setTimeout(() => {
        this.isAdded = false;
        this.newNameGenre = '';
        this.isSaveGenre = true;
      }, 1000);
    } else {
      this.isSaveGenre = false;
    }
  }

  deleteGenre(): void {
    if (this.selectedGenre?.id)
      this.genreService.deleteGenre(this.selectedGenre?.id).subscribe({
        next: (data) => {
          this.isDeleted = true;
          this.getGenres();
        },
        error: (err) => {
          this.content = err;
        },
      });
    setTimeout(() => {
      this.isDeleted = false;
      this.content = undefined;
      this.deSelectGenre();
    }, 1000);
  }

  isSafeString(input: string): boolean {
    const xssPattern = /[<>&"'();%?#]/g;
    const csrfPattern = /[{}/\\]/g;

    const hasXSSChars = xssPattern.test(input);
    const hasCSRFChars = csrfPattern.test(input);

    return !(hasXSSChars || hasCSRFChars);
  }
}
