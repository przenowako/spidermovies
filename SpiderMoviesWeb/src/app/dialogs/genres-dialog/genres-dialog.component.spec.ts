import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GenresDialogComponent } from './genres-dialog.component';
import { FormsModule } from '@angular/forms';

describe('GenresDialogComponent', () => {
  let component: GenresDialogComponent;
  let fixture: ComponentFixture<GenresDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgMultiSelectDropDownModule,
        FormsModule,
      ],
      declarations: [GenresDialogComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(GenresDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create isAdded', () => {
    expect(component.isAdded).toBeFalsy();
  });

  it('should create isDeleted', () => {
    expect(component.isDeleted).toBeFalsy();
  });

  it('should create isUpdated', () => {
    expect(component.isUpdated).toBeFalsy();
  });

  it('should create dropdownSettings', () => {
    expect(component.dropdownSettings).toEqual({
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Wszystkie',
      unSelectAllText: 'Wszystkie',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      noDataAvailablePlaceholderText: 'Brak danych',
      searchPlaceholderText: 'Szukaj',
      closeDropDownOnSelection: true,
    });
  });
});
