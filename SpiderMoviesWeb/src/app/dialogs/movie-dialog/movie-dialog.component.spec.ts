import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { Movie } from '../../model/movie';
import { MovieDialogComponent } from './movie-dialog.component';
import '@angular/common/locales/global/pl';

describe('MovieDialogComponent', () => {
  let component: MovieDialogComponent;
  let fixture: ComponentFixture<MovieDialogComponent>;
  let fakeMovie: Movie = { genres: undefined };
  let fakeCurrentData: any = new DatePipe('pl-PL').transform(
    new Date(),
    'YYYY-MM-dd'
  );

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MovieDialogComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        MatIconModule,        
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MovieDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create movie', () => {
    expect(component.movie).toEqual(fakeMovie);
  });

  it('should create errorMessage', () => {
    expect(component.errorMessage).toEqual('');
  });

  it('should create isSuccessful', () => {
    expect(component.isSuccessful).toBeFalsy();
  });

  it('should create isNew', () => {
    expect(component.isNew).toBeFalsy();
  });

  it('should create currentDate', () => {
    expect(component.currentDate).toBe(fakeCurrentData);
  });

  it('should create dropdownSettings', () => {
    expect(component.dropdownSettings).toEqual({
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Wszystkie',
      unSelectAllText: 'Wszystkie',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      noDataAvailablePlaceholderText: 'Brak danych',
      searchPlaceholderText: 'Szukaj',
    });
  });
});
