import { Component, Inject, Input, OnInit } from '@angular/core';
import { Genres, Movie } from '../../model/movie';
import { MovieService } from '../../services/movie.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-update-movie-dialog',
  templateUrl: './movie-dialog.component.html',
  styleUrls: ['./movie-dialog.component.scss'],
})
export class MovieDialogComponent implements OnInit {
  @Input() movie: Movie = {};
  errorMessage = '';
  isSuccessful = false;
  allGenres?: Genres[];
  selectedGenresNames: string[] = [];
  content?: string;
  isNew = false;
  currentDate: any = new DatePipe('pl-PL').transform(new Date(), 'YYYY-MM-dd');

  constructor(
    public dialogRef: MatDialogRef<MovieDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private movieService: MovieService
  ) {}

  ngOnInit(): void {
    this.isNew = this.data.isNew;
    if (!this.isNew) {
      this.movie = Object.assign({}, this.data.movie);
      this.selectedGenresNames = (this.movie.genres || [])
        .map((genre) => genre.name)
        .filter((name) => name !== undefined) as string[];
    }
    this.getGenres();
  }

  getGenres(): void {
    this.movieService.getGenres().subscribe({
      next: (data) => {
        this.allGenres = data;
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
      },
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.isNew) this.addMovie();
    else this.updateMovie();
  }

  reloadPage(): void {
    window.location.reload();
  }

  addMovie(): void {
    this.movieService.addMovie(this.movie).subscribe({
      next: (data) => {
        this.isSuccessful = true;
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
      },
    });
    this.isSuccessful = false;
    setTimeout(() => this.reloadPage(), 3000);
  }

  updateMovie(): void {
    this.movieService.updateMovie(this.movie.id!, this.movie).subscribe({
      next: (data) => {
        this.isSuccessful = true;
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
      },
    });

    this.isSuccessful = false;
    setTimeout(() => this.reloadPage(), 3000);
  }

  compareGenre(g1: any, g2: any) {
    return g1.name === g2.name;
  }
}
