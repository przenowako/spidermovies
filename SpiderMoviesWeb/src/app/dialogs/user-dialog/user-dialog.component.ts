import { Component, OnInit, Inject, Input } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { User, UserPasword } from '../../model/user';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss'],
})
export class UserDialogComponent implements OnInit {
  @Input() user?: User;
  newUserPassword: UserPasword = {
    userId: -1,
    currentPassword: '',
    newPassword: '',
  };
  errorMessage = '';
  isSuccessful = false;
  content?: string;
  isDetails = false;
  isPassword = false;
  isEmail = false;
  isQrCode = false;
  confirmPassword?: string;
  passwordsNotMatch = true;
  qrCodeUrl: any;
  qrCode: number | null = null;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isDetails = this.data.isDetails;
    this.isPassword = this.data.isPassword;
    this.isEmail = this.data.isEmail;
    this.isQrCode = this.data.isQrCode;
    this.user = Object.assign({}, this.data.user);
    this.newUserPassword.userId = this.user?.id!;
    if (this.isQrCode) this.generateQrCode();
  }

  onSubmit(): void {
    if (this.isDetails) this.updateDeatilsUser();
    if (this.isPassword) this.updatePasswordUser();
    if (this.isEmail) this.updateEmailUser();
    if (this.isQrCode) this.changeQrCode();
  }

  checkPasswords(): void {
    if (this.confirmPassword === this.newUserPassword.newPassword)
      this.passwordsNotMatch = false;
    else this.passwordsNotMatch = true;
  }

  updateDeatilsUser(): void {
    this.userService.updateUser(this.user!).subscribe({
      next: (data) => {
        this.isSuccessful = true;
        if (this.isSuccessful)
          setTimeout(() => this.dialogRef.close(this.isSuccessful), 1000);
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
      },
    });
    this.isSuccessful = false;
  }

  updatePasswordUser(): void {
    this.checkPasswords();

    if (this.passwordsNotMatch) {
      this.dialog.open(AlertDialogComponent, {
        data: { passwordsNotMatch: this.passwordsNotMatch },
      });
    } else {
      this.authService.updatePasswordUser(this.newUserPassword!).subscribe({
        next: (data) => {
          this.isSuccessful = true;
          if (this.isSuccessful) {
            setTimeout(() => {
              this.dialogRef.close(this.isSuccessful);
            }, 5000);
          }
        },
        error: (err) => {
          if (err.error) {
            try {
              const res = JSON.parse(err.error);
              this.content = res.message;
            } catch {
              this.content = `Error with status: ${err.status} - ${err.statusText}`;
            }
          } else {
            this.content = `Error with status: ${err.status}`;
          }
          this.dialog.open(AlertDialogComponent, {
            data: { passwordNotUpdated: true },
          });
        },
      });
      this.isSuccessful = false;
    }
  }

  updateEmailUser(): void {
    this.userService.updateUser(this.user!).subscribe({
      next: (data) => {
        this.isSuccessful = true;
        if (this.isSuccessful)
          setTimeout(() => this.dialogRef.close(this.isSuccessful), 1000);
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
        this.dialog.open(AlertDialogComponent, {
          data: { emailInUse: true },
        });
      },
    });
    this.isSuccessful = false;
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: (res) => {
        this.userService.clean();

        window.location.replace('/');
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  changeQrCode(): void {
    this.authService.validate(this.user!.username, this.qrCode!).subscribe({
      next: (data) => {
        this.isSuccessful = data;
        if (this.isSuccessful)
          setTimeout(() => this.dialogRef.close(this.isSuccessful), 1000);
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
      },
    });
    this.isSuccessful = false;
  }

  generateQrCode(): void {
    this.authService
      .generate(this.user!.username)
      .subscribe((qrCodeBlob: Blob) => {
        this.createImageUrl(qrCodeBlob);
      });
  }

  createImageUrl(qrCodeBlob: Blob) {
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.qrCodeUrl = reader.result;
      },
      false
    );

    if (qrCodeBlob) {
      reader.readAsDataURL(qrCodeBlob);
    }
  }
}
