import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserDialogComponent } from './user-dialog.component';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { UserPasword } from '../../model/user';

describe('UserDialogComponent', () => {
  let component: UserDialogComponent;
  let fixture: ComponentFixture<UserDialogComponent>;
  let fakeNewUserPassword: UserPasword = {
    userId: -1,
    currentPassword: '',
    newPassword: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserDialogComponent],
      imports: [HttpClientTestingModule, FormsModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
        { provide: MatDialog, useValue: [] },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create errorMessage', () => {
    expect(component.errorMessage).toEqual('');
  });

  it('should create isSuccessful', () => {
    expect(component.isSuccessful).toBeFalsy();
  });

  it('should create isDetails', () => {
    expect(component.isDetails).toBeFalsy();
  });

  it('should create isPassword', () => {
    expect(component.isPassword).toBeFalsy();
  });

  it('should create isEmail', () => {
    expect(component.isEmail).toBeFalsy();
  });

  it('should create passwordsNotMatch', () => {
    expect(component.passwordsNotMatch).toBeTruthy();
  });

  it('should create newUserPassword', () => {
    expect(component.newUserPassword.userId).toBeUndefined();
    expect(component.newUserPassword.currentPassword).toEqual(
      fakeNewUserPassword.currentPassword
    );
    expect(component.newUserPassword.newPassword).toEqual(
      fakeNewUserPassword.newPassword
    );
  });
});
