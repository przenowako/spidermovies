import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss'],
})
export class AlertDialogComponent implements OnInit {
  isRoleUpdated = false;
  passwordNotMatch = false;
  passwordNotUpdated = false;
  emailInUse = false;

  constructor(
    public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    if(this.data.passwordsNotMatch) this.passwordNotMatch = this.data.passwordsNotMatch;
    if(this.data.isRoleUpdated) this.isRoleUpdated = this.data.isRoleUpdated;
    if(this.data.passwordNotUpdated) this.passwordNotUpdated = this.data.passwordNotUpdated;
    if(this.data.emailInUse) this.emailInUse = this.data.emailInUse;
  }
}
