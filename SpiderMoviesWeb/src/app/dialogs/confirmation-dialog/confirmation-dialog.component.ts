import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  movieContent: boolean = false;
  userContent: boolean = false;
  isWaiting: boolean = false;
  isOk: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      movieContent: boolean;
      userContent: boolean;
      isWaiting: boolean;
      isOk: boolean;
    }
  ) {}

  ngOnInit(): void {
    this.movieContent = this.data.movieContent;
    this.userContent = this.data.userContent;
    this.isWaiting = this.data.isWaiting;
    this.isOk = this.data.isOk;
  }

  onYesClick(): void {
    this.dialogRef.close(true);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
