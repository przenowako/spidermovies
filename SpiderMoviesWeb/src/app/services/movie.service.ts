import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie, Genres, MoviesResponse } from '../model/movie';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { UserService } from './user.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  private apiServerUrl = environment.apiBaseUrl;
  private apiGenresUrl = environment.apiGenresUrl;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    if (this.userService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.userService.getUserSession().roles;
    }
  }

  public getMovies(params: any): Observable<MoviesResponse> {
    return this.http.get<MoviesResponse>(this.apiServerUrl, { params });
  }

  public getMovieUser(id: number, userId: number): Observable<Movie> {
    return this.http.get<Movie>(
      `${this.apiServerUrl}/user/${id}?userid=${userId}`,
      httpOptions
    );
  }

  public getMovie(id: number): Observable<Movie> {
    return this.http.get<Movie>(`${this.apiServerUrl}/${id}`);
  }

  public addMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(
      `${this.apiServerUrl}/add`,
      movie,
      httpOptions
    );
  }

  public updateMovie(id: number, movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(
      `${this.apiServerUrl}/${id}`,
      movie,
      httpOptions
    );
  }

  public deleteMovie(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${id}`, httpOptions);
  }

  public getGenres(): Observable<Genres[]> {
    return this.http.get<Genres[]>(`${this.apiGenresUrl}/all`);
  }
}
