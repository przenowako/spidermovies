import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserPasword } from '../model/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private apiAuthUrl = environment.apiAuthUrl;
  constructor(private http: HttpClient) {}

  login(username: string, password: string, qrCode: number): Observable<any> {
    return this.http.post(
      this.apiAuthUrl + '/signin',
      {
        username,
        password,
        qrCode,
      },
      httpOptions
    );
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(
      this.apiAuthUrl + '/signup',
      {
        username,
        email,
        password,
      },
      httpOptions
    );
  }

  logout(): Observable<any> {
    return this.http.post(this.apiAuthUrl + '/signout', {}, httpOptions);
  }

  updatePasswordUser(userPassword: UserPasword): Observable<any> {
    return this.http.put<any>(
      `${this.apiAuthUrl}/updatePassword`,
      userPassword,
      httpOptions
    );
  }

  generate(username: string): Observable<any> {
    return this.http.get(`${this.apiAuthUrl}/generate/${username}`, {
      responseType: 'blob',
    });
  }

  validate(username: string, code: number): Observable<any> {
    return this.http.post(
      this.apiAuthUrl + '/validate',
      {
        username,
        code,
      },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
    );
  }
}
