import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyFromClass, Spy } from 'jasmine-auto-spies';
import { User, UserPasword } from '../model/user';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let httpSpy: Spy<HttpClient>;
  let fakeUser: User = {
    id: 1,
    username: 'fakeUsername',
    email: 'fakeEmail',
    password: 'Fake1@',
    name: 'fakeName',
    lastName: 'fakeLastName',
    address: 'fakeAddress',
    role: { id: 1, name: 'USER' },
  };
  let fakeUserPassword: UserPasword = {
    userId: 1,
    currentPassword: 'Fakecurrent1@',
    newPassword: 'Fakecurrent2@',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        AuthService,
        { provide: HttpClient, useValue: createSpyFromClass(HttpClient) },
      ],
    });
    service = TestBed.inject(AuthService);
    httpSpy = TestBed.inject<any>(HttpClient);
  });

  describe('login', () => {
    it('should login', (done: DoneFn) => {
      httpSpy.post.and.nextWith(fakeUser);
      service.login(fakeUser.username, fakeUser.password).subscribe((user) => {
        expect(user).toBe(fakeUser);
        done();
      }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('register', () => {
    it('should register', (done: DoneFn) => {
      httpSpy.post.and.nextWith(fakeUser);
      service
        .register(fakeUser.username, fakeUser.email, fakeUser.password)
        .subscribe((user) => {
          expect(user).toBe(fakeUser);
          done();
        }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('logout', () => {
    it('should logout', (done: DoneFn) => {
      httpSpy.post.and.nextWith('Zostałeś wylogowany');
      service
        .register(fakeUser.username, fakeUser.email, fakeUser.password)
        .subscribe((response) => {
          expect(response).toBe('Zostałeś wylogowany');
          done();
        }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('updatePasswordUser', () => {
    it('should update password', (done: DoneFn) => {
      httpSpy.put.and.nextWith(fakeUserPassword);
      service.updatePasswordUser(fakeUserPassword).subscribe((user) => {
        expect(user).toBe(fakeUserPassword);
        done();
      }, done.fail);

      expect(httpSpy.put.calls.count()).toBe(1);
    });
  });
});
