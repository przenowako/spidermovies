import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Role, User, UserPasword, UsersResponse } from '../model/user';
import { environment } from 'src/environments/environment';
import { Movie, MoviesResponse } from '../model/movie';
import { Favorite } from '../model/favorite';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private apiUsersUrl = environment.apiUsersUrl;
  private apiRoleUrl = environment.apiRoleUrl;
  constructor(private http: HttpClient) {}

  clean(): void {
    window.sessionStorage.clear();
  }

  saveUser(user: any): void {
    window.sessionStorage.removeItem('auth-user');
    window.sessionStorage.setItem('auth-user', JSON.stringify(user));
  }

  getUserSession(): any {
    const user = window.sessionStorage.getItem('auth-user');
    if (user) return JSON.parse(user);

    return {};
  }

  isLoggedIn(): boolean {
    const user = window.sessionStorage.getItem('auth-user');
    if (user) return true;

    return false;
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.apiUsersUrl}/${id}`, httpOptions);
  }

  getUsers(params: any): Observable<UsersResponse> {
    return this.http.get<UsersResponse>(this.apiUsersUrl, {
      params,
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true,
    });
  }

  deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUsersUrl}/${id}`, httpOptions);
  }

  updateUserRole(id: number, user: User): Observable<any> {
    return this.http.post<any>(
      `${this.apiUsersUrl}/setRole/${id}`,
      user,
      httpOptions
    );
  }

  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.apiRoleUrl, httpOptions);
  }

  getfavoriteMovies(id: any): Observable<MoviesResponse> {
    return this.http.get<MoviesResponse>(
      `${this.apiUsersUrl}/favoriteMovies/${id}`,
      httpOptions
    );
  }

  deleteFavoriteMovie(favorite: Favorite): Observable<any> {
    return this.http.put<void>(
      `${this.apiUsersUrl}/favoriteMovies`,
      favorite,
      httpOptions
    );
  }

  setFavoriteMovie(favorite: Favorite): Observable<Movie> {
    return this.http.post<Movie>(
      `${this.apiUsersUrl}/favoriteMovies`,
      favorite,
      httpOptions
    );
  }

  updateUser(user: User): Observable<any> {
    return this.http.put<any>(
      `${this.apiUsersUrl}/${user.id}`,
      user,
      httpOptions
    );
  }
}
