import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyFromClass, Spy } from 'jasmine-auto-spies';
import { Favorite } from '../model/favorite';
import { Movie, MoviesResponse } from '../model/movie';
import { Role, User, UsersResponse } from '../model/user';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let httpSpy: Spy<HttpClient>;
  let fakeUser: User = {
    id: 1,
    username: 'fakeUsername',
    email: 'fakeEmail',
    password: 'Fake1@',
    name: 'fakeName',
    lastName: 'fakeLastName',
    address: 'fakeAddress',
    role: { id: 1, name: 'USER' },
  };
  let fakeUsers: User[] = [fakeUser, fakeUser];
  let fakeUsersResponse: UsersResponse = {
    users: fakeUsers,
    currentPage: 1,
    totalItems: 1,
    totalPages: 1,
  };
  let fakeRoles: Role[] = [
    { id: 1, name: 'USER' },
    { id: 2, name: 'ADMIN' },
  ];
  let fakeMovie: Movie = {
    id: 1,
    title: 'test1',
    author: 'test2',
    image: 'image1',
    price: 11.11,
    nextDayFee: 22.22,
    releaseDate: new Date('2002-01-18'),
    ageClassification: 6,
    sourceLink: 'source_link1',
    description: 'dest',
    genres: [
      { id: 1, name: 'genre1' },
      { id: 2, name: 'genre2' },
    ],
  };
  let fakeMovies: Movie[] = [
    {
      id: 1,
      title: 'test1',
      author: 'test2',
      image: 'image1',
      price: 11.11,
      nextDayFee: 22.22,
      releaseDate: new Date('2002-01-18'),
      ageClassification: 6,
      sourceLink: 'source_link1',
      description: 'dest',
      genres: [
        { id: 1, name: 'genre1' },
        { id: 2, name: 'genre2' },
      ],
    },
  ];
  let fakeMovieResponse: MoviesResponse = {
    movies: fakeMovies,
    currentPage: 1,
    totalItems: 1,
    totalPages: 1,
  };
  let fakeFavorite: Favorite = {
    userId: fakeUser.id,
    movie_id: fakeMovie.id,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        UserService,
        { provide: HttpClient, useValue: createSpyFromClass(HttpClient) },
      ],
    });
    service = TestBed.inject(UserService);
    httpSpy = TestBed.inject<any>(HttpClient);
  });

  describe('getUser', () => {
    it('should return user', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeUser);
      service.getUser(fakeUser.id!).subscribe((user) => {
        expect(user).toBe(fakeUser);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('getUsers', () => {
    it('should return users', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeUsersResponse);
      let params: any = {};
      service.getUsers(params).subscribe((users) => {
        expect(users).toBe(fakeUsersResponse);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('deleteUser', () => {
    it('should delete user', (done: DoneFn) => {
      httpSpy.delete.and.nextWith(fakeUser);
      service.deleteUser(fakeUser.id!).subscribe((user) => {
        expect(user).toBeTruthy();
        done();
      }, done.fail);

      expect(httpSpy.delete.calls.count()).toBe(1);
    });
  });

  describe('updateUserRole', () => {
    it('should update user role', (done: DoneFn) => {
      httpSpy.post.and.nextWith(fakeUser);
      service.updateUserRole(fakeUser.id!, fakeUser).subscribe((user) => {
        expect(user).toBe(fakeUser);
        done();
      }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('getRoles', () => {
    it('should return roles', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeRoles);
      service.getRoles().subscribe((user) => {
        expect(user).toBe(fakeRoles);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('getfavoriteMovies', () => {
    it('should return favorite movies', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeMovieResponse);
      service.getfavoriteMovies(1).subscribe((movies) => {
        expect(movies).toBe(fakeMovieResponse);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('deleteFavoriteMovie', () => {
    it('should delete favorite movie', (done: DoneFn) => {
      httpSpy.put.and.nextWith(fakeFavorite);
      service.deleteFavoriteMovie(fakeFavorite).subscribe((movie) => {
        expect(movie).toBe(fakeFavorite);
        done();
      }, done.fail);

      expect(httpSpy.put.calls.count()).toBe(1);
    });
  });

  describe('setFavoriteMovie', () => {
    it('should set favorite movie', (done: DoneFn) => {
      httpSpy.post.and.nextWith(fakeMovie);
      service.setFavoriteMovie(fakeFavorite).subscribe((movie) => {
        expect(movie).toBe(fakeMovie);
        done();
      }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('updateUser', () => {
    it('should update user', (done: DoneFn) => {
      httpSpy.put.and.nextWith(fakeUser);
      service.updateUser(fakeUser).subscribe((user) => {
        expect(user).toBe(fakeUser);
        done();
      }, done.fail);

      expect(httpSpy.put.calls.count()).toBe(1);
    });
  });
});
