import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyFromClass, Spy } from 'jasmine-auto-spies';
import { Genres, Movie } from '../model/movie';
import { MovieService } from './movie.service';

describe('MovieService', () => {
  let service: MovieService;
  let httpSpy: Spy<HttpClient>;
  let fakeGenres: Genres[] = [
    {
      id: 1,
      name: 'test genre1',
    },
    { id: 2, name: 'test genre2' },
  ];
  let fakeMovie: Movie = {
    id: 1,
    title: 'test1',
    author: 'test2',
    image: 'image1',
    price: 11.11,
    nextDayFee: 22.22,
    releaseDate: new Date('2002-01-18'),
    ageClassification: 6,
    sourceLink: 'source_link1',
    description: 'dest',
    genres: fakeGenres,
  };
  let fakeMovies: Movie[] = [fakeMovie, fakeMovie];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        MovieService,
        { provide: HttpClient, useValue: createSpyFromClass(HttpClient) },
      ],
    });
    service = TestBed.inject(MovieService);
    httpSpy = TestBed.inject<any>(HttpClient);
  });

  describe('getMovies', () => {
    it('should return page of movies', (done: DoneFn) => {
      const params: any = {};
      httpSpy.get.and.nextWith(fakeMovies);
      service.getMovies(params).subscribe((movies) => {
        expect(movies).toHaveSize(fakeMovies.length);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('getMovieUser', () => {
    it('should return movie without all data', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeMovie);
      service.getMovieUser(1, 1).subscribe((movie) => {
        expect(movie).toBe(fakeMovie);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('getMovie', () => {
    it('should return movie', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeMovie);
      service.getMovie(1).subscribe((movie) => {
        expect(movie).toBe(fakeMovie);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });

  describe('addMovie', () => {
    it('should add movie', (done: DoneFn) => {
      httpSpy.post.and.nextWith(fakeMovies[1]);
      service.addMovie(fakeMovies[1]).subscribe((movie) => {
        expect(movie).toBe(fakeMovies[1]);
        done();
      }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('updateMovie', () => {
    it('should update movie', (done: DoneFn) => {
      httpSpy.put.and.nextWith(fakeMovie);
      service.updateMovie(fakeMovie.id!, fakeMovies[1]).subscribe((movie) => {
        expect(movie).toBe(fakeMovie);
        done();
      }, done.fail);

      expect(httpSpy.put.calls.count()).toBe(1);
    });
  });

  describe('deleteMovie', () => {
    it('should delete movie', (done: DoneFn) => {
      httpSpy.delete.and.nextWith(fakeMovie);
      service.deleteMovie(fakeMovie.id!).subscribe((movie) => {
        expect(movie).toBeTruthy();
        done();
      }, done.fail);

      expect(httpSpy.delete.calls.count()).toBe(1);
    });
  });

  describe('getGenres', () => {
    it('should return genres', (done: DoneFn) => {
      httpSpy.get.and.nextWith(fakeGenres);
      service.getGenres().subscribe((genres) => {
        expect(genres).toBe(fakeGenres);
        done();
      }, done.fail);

      expect(httpSpy.get.calls.count()).toBe(1);
    });
  });
});
