import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { createSpyFromClass, Spy } from 'jasmine-auto-spies';
import { Genres } from '../model/movie';
import { GenresService } from './genres.service';

describe('GenresService', () => {
  let service: GenresService;
  let httpSpy: Spy<HttpClient>;
  let fakeGenre: Genres = {
    id: 1,
    name: 'fake',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        GenresService,
        { provide: HttpClient, useValue: createSpyFromClass(HttpClient) },
      ],
    });
    service = TestBed.inject(GenresService);
    httpSpy = TestBed.inject<any>(HttpClient);
  });

  describe('addGenre', () => {
    it('should add genre', (done: DoneFn) => {
      httpSpy.post.and.nextWith(fakeGenre);
      service.addGenre(fakeGenre.name!).subscribe((genre) => {
        expect(genre).toBe(fakeGenre);
        done();
      }, done.fail);

      expect(httpSpy.post.calls.count()).toBe(1);
    });
  });

  describe('deleteGenre', () => {
    it('should delete genre', (done: DoneFn) => {
      httpSpy.delete.and.nextWith(fakeGenre);
      service.deleteGenre(fakeGenre.id!).subscribe((genre) => {
        expect(genre).toBeTruthy();
        done();
      }, done.fail);

      expect(httpSpy.delete.calls.count()).toBe(1);
    });
  });

  describe('updateGenre', () => {
    it('should update genre', (done: DoneFn) => {
      httpSpy.put.and.nextWith(fakeGenre);
      service.updateGenre(fakeGenre).subscribe((genre) => {
        expect(genre).toBe(fakeGenre);
        done();
      }, done.fail);

      expect(httpSpy.put.calls.count()).toBe(1);
    });
  });
});
