import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genres } from '../model/movie';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root',
})
export class GenresService {
  private apiGenresUrl = environment.apiGenresUrl;

  constructor(private http: HttpClient) {}

  public addGenre(nameGenre: String): Observable<Genres> {
    return this.http.post<Genres>(
      `${this.apiGenresUrl}/add`,
      nameGenre,
      httpOptions
    );
  }

  public updateGenre(genre: Genres): Observable<Genres> {
    return this.http.put<Genres>(
      `${this.apiGenresUrl}/${genre.id}`,
      genre,
      httpOptions
    );
  }

  public deleteGenre(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiGenresUrl}/${id}`, httpOptions);
  }
}
