import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ConfirmationDialogComponent } from '../dialogs/confirmation-dialog/confirmation-dialog.component';
import { User } from '../model/user';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { EventBusService } from '../shared/event-bus.service';
import { UserDialogComponent } from '../dialogs/user-dialog/user-dialog.component';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
})
export class ProfilComponent implements OnInit {
  currentUser?: User;
  eventBusSub?: Subscription;
  isLoggedIn = false;
  user?: User;

  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private eventBusService: EventBusService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.userService.isLoggedIn();
    this.currentUser = this.userService.getUserSession();
    this.getUserDetails();
    this.eventBusSub = this.eventBusService.on('logout', () => {
      this.logout();
    });
  }

  getUserDetails(): void {
    this.userService.getUser(Number(this.currentUser?.id)).subscribe({
      next: (data) => {
        this.user = data;
      },
    });
  }

  deleteUser(id: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        movieContent: false,
        userContent: true,
        isWaiting: false,
        isOk: false,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userService.deleteUser(id).subscribe(() => {
          this.logout();
        });
      }
    });
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: (res: any) => {
        this.userService.clean();
        window.location.replace('/');
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  openUpdateDetailsUserDialog(): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      panelClass: 'dialog',
      data: { user: this.user, isDetails: true },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) this.getUserDetails();
    });
  }

  openUpdatePasswordUserDialog(): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      panelClass: 'dialog',
      data: { user: this.user, isPassword: true },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) this.getUserDetails();
    });
  }

  openUpdateEmailUserDialog(): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      panelClass: 'dialog',
      data: { user: this.user, isEmail: true },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) this.getUserDetails();
    });
  }

  openChangeQrCodeDialog(): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      panelClass: 'dialog',
      data: { user: this.user, isQrCode: true },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) this.getUserDetails();
    });
  }
}
