import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { MovieComponent } from './movie.component';

describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;
  let activatedRouteSpy;

  beforeEach(async () => {
    activatedRouteSpy = {
      snapshot: {
        paramMap: convertToParamMap({
          id: '1',
        }),
      },
    };
    await TestBed.configureTestingModule({
      declarations: [MovieComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: MatDialog, useValue: {} },
        { provide: ActivatedRoute, useValue: [] },
        {
          provide: ActivatedRoute,
          useValue: activatedRouteSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create isAdmin', () => {
    expect(component.isAdmin).toBeFalsy();
  });

  it('should create isManager', () => {
    expect(component.isManager).toBeFalsy();
  });

  it('should create isLoggedIn', () => {
    expect(component.isLoggedIn).toBeFalsy();
  });

  it('should create isDeleted', () => {
    expect(component.isDeleted).toBeFalsy();
  });

  it('should create isAccepted', () => {
    expect(component.isAccepted).toBeFalsy();
  });

  it('should create isRent', () => {
    expect(component.isRent).toBeFalsy();
  });

  it('should create roles', () => {
    expect(component.roles).toEqual([]);
  });
});
