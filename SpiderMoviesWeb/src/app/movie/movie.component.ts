import { Component, OnInit } from '@angular/core';
import { Movie } from '../model/movie';
import { MovieService } from '../services/movie.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MovieDialogComponent } from '../dialogs/movie-dialog/movie-dialog.component';
import { ConfirmationDialogComponent } from '../dialogs/confirmation-dialog/confirmation-dialog.component';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent implements OnInit {
  content?: string;
  isAdmin = false;
  isManager = false;
  isLoggedIn = false;
  roles: string[] = [];
  movie?: Movie;
  movie_id?: number;
  user_id?: number;
  isDeleted = false;
  isAccepted = false;

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.movie_id = Number(this.route.snapshot.paramMap.get('id'));
    this.isLoggedIn = this.userService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.userService.getUserSession();
      this.roles = user.roles;

      this.isAdmin = this.roles.includes('ADMIN');
      this.isManager = this.roles.includes('MANAGER');
      this.user_id = user.id;
    }

    this.getMovie();
  }

  getMovie(): void {
    if (this.user_id)
      this.movieService.getMovieUser(this.movie_id!, this.user_id).subscribe({
        next: (data) => {
          this.movie = data;
        },
      });
    else
      this.movieService.getMovie(this.movie_id!).subscribe({
        next: (data) => {
          this.movie = data;
        },
      });
  }

  openUpdateMovieDialog(): void {
    const dialogRef = this.dialog.open(MovieDialogComponent, {
      panelClass: 'dialog',
      data: { movie: this.movie, isNew: false },
    });
  }

  deleteMovie(id: number): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        movieContent: true,
        userContent: false,
        isWaiting: false,
        isOk: false,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.movieService.deleteMovie(this.movie_id!).subscribe(() => {
          this.isDeleted = true;
          this.reloadPage();
        });
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
    window.location.replace('/');
  }
}
