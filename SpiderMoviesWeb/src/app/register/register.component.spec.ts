import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../model/user';
import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let fakeForm: User = {
    username: '',
    email: '',
    password: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [HttpClientTestingModule, FormsModule],
      providers: [{ provide: MatDialog, useValue: {} }],
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create isSuccessful', () => {
    expect(component.isSuccessful).toBeFalsy();
  });

  it('should create isSignUpFailed', () => {
    expect(component.isSignUpFailed).toBeFalsy();
  });

  it('should create passwordsNotMatch', () => {
    expect(component.passwordsNotMatch).toBeTruthy();
  });

  it('should create errorMessage', () => {
    expect(component.errorMessage).toEqual('');
  });

  it('should create form', () => {
    expect(component.form).toEqual(fakeForm);
  });
});
