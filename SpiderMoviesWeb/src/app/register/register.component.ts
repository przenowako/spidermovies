import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogComponent } from '../dialogs/alert-dialog/alert-dialog.component';
import { User } from '../model/user';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form: User = {
    username: '',
    email: '',
    password: '',
  };
  confirmPassword?: string;

  isSignUpSuccessful = false;
  passwordsNotMatch = true;
  errorMessage = '';
  isLoggedIn = false;
  qrCodeUrl: any;
  qrCode: number | null = null;
  qrCodeResponse: boolean = false;
  qrCodeInvalid: boolean = false;

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.userService.isLoggedIn();
  }

  onSubmit(): void {
    const { username, email, password } = this.form;

    this.checkPasswords();

    if (this.passwordsNotMatch) {
      this.dialog.open(AlertDialogComponent, {
        data: { passwordsNotMatch: this.passwordsNotMatch },
      });
    } else {
      this.authService.register(username, email, password).subscribe({
        next: (data) => {
          this.isSignUpSuccessful = true;
          this.generateQrCode();
        },
        error: (err) => {
          this.errorMessage = err.error.message;
          this.isSignUpSuccessful = false;
        },
      });
    }
  }

  checkPasswords(): void {
    if (this.confirmPassword === this.form.password)
      this.passwordsNotMatch = false;
    else this.passwordsNotMatch = true;
  }

  reloadPage(): void {
    window.location.reload();
    window.location.replace('/login');
  }

  generateQrCode(): void {
    this.authService
      .generate(this.form.username)
      .subscribe((qrCodeBlob: Blob) => {
        this.createImageUrl(qrCodeBlob);
      });
  }

  createImageUrl(qrCodeBlob: Blob) {
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.qrCodeUrl = reader.result;
      },
      false
    );

    if (qrCodeBlob) {
      reader.readAsDataURL(qrCodeBlob);
    }
  }

  checkQrCode(): void {
    if (this.qrCode == null) {
      this.qrCodeInvalid = true;
    } else {
      this.authService.validate(this.form.username, this.qrCode!).subscribe({
        next: (data) => {
          this.qrCodeResponse = data;
          if (this.qrCodeResponse) {
            setTimeout(() => this.reloadPage(), 3000);
            this.qrCodeInvalid = false;
          } else this.qrCodeInvalid = true;
        },
        error: (err) => {
          console.log(err);
          this.errorMessage = err.error.message;
          this.qrCodeResponse = false;
        },
      });
    }
  }
}
