import { Component, OnInit } from '@angular/core';
import { Favorite } from '../../model/favorite';
import { Movie } from '../../model/movie';
import { User } from '../../model/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-favorite-movies-list',
  templateUrl: './favorite-movies-list.component.html',
  styleUrls: ['./favorite-movies-list.component.scss'],
})
export class FavoriteMoviesListComponent implements OnInit {
  isLogged: boolean = false;
  currentUser?: User;
  favoriteMovies: Movie[] = [];
  favoriteMovie?: Favorite;
  count = 0;
  pageSize = 4;
  pageSizes = [4, 8, 12, 16, 20];
  page = 1;
  displayedColumns = ['id', 'title', 'author', 'year'];
  tableDef: Array<any> = [
    {
      key: 'id',
      header: 'ID',
    },
    {
      key: 'title',
      header: 'Tytuł',
    },
    {
      key: 'author',
      header: 'Autor',
    },
    {
      key: 'year',
      header: 'Rok',
    },
  ];
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.isLogged = this.userService.isLoggedIn();
    this.currentUser = this.userService.getUserSession();
    this.getFavoriteMovies();
  }

  getFavoriteMovies(): void {
    this.userService.getfavoriteMovies(this.currentUser?.id).subscribe({
      next: (data) => {
        const { movies, totalItems } = data;
        this.favoriteMovies = movies!;
        this.count = totalItems!;
      },
    });
  }

  changeColumnHeader(column: string): string {
    let header = column;

    this.tableDef.map((el) => {
      if (el.key === column) header = el.header;
    });
    return header;
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.getFavoriteMovies();
  }

  handlePageSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
    this.getFavoriteMovies();
  }

  addToFavorite(favoriteMovie: Favorite): void {
    this.userService.setFavoriteMovie(favoriteMovie).subscribe({
      next: (data) => {
        this.getFavoriteMovies();
      },
    });
  }

  removeFromFavorite(favoriteMovie: Favorite): void {
    this.userService.deleteFavoriteMovie(favoriteMovie).subscribe({
      next: (data) => {
        this.getFavoriteMovies();
      },
    });
  }

  updateFavoriteMovie(id: any): void {
    this.favoriteMovie = {
      movieId: id,
      userId: Number(this.currentUser?.id!),
    };
    if (this.isFavorite(id)) this.removeFromFavorite(this.favoriteMovie);
    else this.addToFavorite(this.favoriteMovie);
  }

  isFavorite(id: any): boolean {
    if (this.favoriteMovies.find((el) => el.id === id)) return true;
    return false;
  }
}
