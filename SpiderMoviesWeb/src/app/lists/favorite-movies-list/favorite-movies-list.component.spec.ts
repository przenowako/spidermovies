import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FavoriteMoviesListComponent } from './favorite-movies-list.component';

describe('FavoriteMoviesListComponent', () => {
  let component: FavoriteMoviesListComponent;
  let fixture: ComponentFixture<FavoriteMoviesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FavoriteMoviesListComponent],
      imports: [HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(FavoriteMoviesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create isLogged', () => {
    expect(component.isLogged).toBeFalsy();
  });

  it('should create currentUser', () => {
    expect(component.isLogged).toBeFalsy();
  });

  it('should create favoriteMovies', () => {
    expect(component.favoriteMovies).toEqual([]);
  });

  it('should create favoriteMovie', () => {
    expect(component.favoriteMovie).toBeUndefined();
  });

  it('should create count', () => {
    expect(component.count).toEqual(0);
  });

  it('should create pageSize', () => {
    expect(component.pageSize).toEqual(4);
  });

  it('should create pageSizes', () => {
    expect(component.pageSizes).toEqual([4, 8, 12, 16, 20]);
  });

  it('should create page', () => {
    expect(component.page).toEqual(1);
  });

  it('should create displayedColumns', () => {
    expect(component.displayedColumns).toEqual([
      'id',
      'title',
      'author',
      'year',
    ]);
  });

  it('should create tableDef', () => {
    expect(component.tableDef).toEqual([
      {
        key: 'id',
        header: 'ID',
      },
      {
        key: 'title',
        header: 'Tytuł',
      },
      {
        key: 'author',
        header: 'Autor',
      },
      {
        key: 'year',
        header: 'Rok',
      },
    ]);
  });

  it('should create isFavorite', () => {
    expect(component.isFavorite(1)).toBeFalsy();
  });

  it('should create changeColumnHeader', () => {
    expect(component.changeColumnHeader('fake')).toBe('fake');
  });
});
