import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UsersListComponent } from './users-list.component';
import { MatDialog } from '@angular/material/dialog';
import { Role, User } from '../../model/user';

describe('UsersListComponent', () => {
  let component: UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;
  let fakeTableDef: Array<any> = [
    {
      key: 'id',
      header: 'ID',
    },
    {
      key: 'username',
      header: 'Nazwa uzytkownika',
    },
    {
      key: 'email',
      header: 'Email',
    },
    {
      key: 'role',
      header: 'Typ',
    },
  ];
  let fakeUsers: User[] = [];
  let fakeRoles: Role[] = [];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [UsersListComponent],
      providers: [{ provide: MatDialog, useValue: [] }],
    }).compileComponents();

    fixture = TestBed.createComponent(UsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create sortingSettings', () => {
    expect(component.sortingSettings).toEqual({
      singleSelection: true,
      idField: 'id',
      textField: 'itemName',
      selectAllText: 'Wszystkie',
      unSelectAllText: 'Wszystkie',
      itemsShowLimit: 1,
      allowSearchFilter: false,
      noDataAvailablePlaceholderText: 'Brak danych',
      searchPlaceholderText: 'Szukaj',
      closeDropDownOnSelection: true,
    });
  });

  it('should create rolesSettings', () => {
    expect(component.rolesSettings).toEqual({
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Wszystkie',
      unSelectAllText: 'Wszystkie',
      itemsShowLimit: 1,
      allowSearchFilter: false,
      noDataAvailablePlaceholderText: 'Brak danych',
      searchPlaceholderText: 'Szukaj',
      closeDropDownOnSelection: true,
    });
  });

  it('should create sortFields', () => {
    expect(component.sortFields).toEqual([
      {
        id: 1,
        itemName: '▲ Nazwa użytkownika',
        itemArray: ['username', 'asc'],
      },
      {
        id: 2,
        itemName: '▼ Nazwa użytkownika',
        itemArray: ['username', 'desc'],
      },
      { id: 3, itemName: '▲ Email', itemArray: ['email', 'asc'] },
      { id: 4, itemName: '▼ Email', itemArray: ['email', 'desc'] },
      { id: 5, itemName: '▲ Typ', itemArray: ['role', 'asc'] },
      { id: 6, itemName: '▼ Typ', itemArray: ['role', 'desc'] },
    ]);
  });

  it('should create params', () => {
    expect(component.params).toEqual(
      component.getRequestParams(1, 4, [], '', NaN)
    );
  });

  it('should create tableDef', () => {
    expect(component.tableDef).toEqual(fakeTableDef);
  });

  it('should create displayedColumns', () => {
    expect(component.displayedColumns).toEqual([
      'id',
      'username',
      'email',
      'role',
    ]);
  });

  it('should create page', () => {
    expect(component.page).toEqual(1);
  });

  it('should create count', () => {
    expect(component.count).toEqual(0);
  });

  it('should create pageSize', () => {
    expect(component.pageSize).toEqual(4);
  });

  it('should create pageSizes', () => {
    expect(component.pageSizes).toEqual([4, 8, 12, 16, 20]);
  });

  it('should create sortName', () => {
    expect(component.sortName).toEqual([]);
  });

  it('should create sort', () => {
    expect(component.sort).toEqual([]);
  });

  it('should create isLogged', () => {
    expect(component.isLogged).toBeFalsy();
  });

  it('should create isRoleUpdated', () => {
    expect(component.isRoleUpdated).toBeFalsy();
  });

  it('should create selectedRoleId', () => {
    expect(component.selectedRoleId).toEqual(NaN);
  });

  it('should create users', () => {
    expect(component.users).toEqual(fakeUsers);
  });

  it('should create roles', () => {
    expect(component.roles).toEqual(fakeRoles);
  });
});
