import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogComponent } from '../../dialogs/alert-dialog/alert-dialog.component';
import { ConfirmationDialogComponent } from '../../dialogs/confirmation-dialog/confirmation-dialog.component';
import { Role, User } from '../../model/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent {
  isLogged: boolean = false;
  isUser = false;
  count = 0;
  content?: string;
  username = '';
  email = '';
  page = 1;
  pageSize = 4;
  pageSizes = [4, 8, 12, 16, 20];
  sort: String[] = [];
  users: User[] = [];
  roles: Role[] = [];
  textSearched = '';
  selectedRole?: Role[];
  selectedRoleId: Number = NaN;
  isRoleUpdated = false;
  displayedColumns = ['id', 'username', 'email', 'role'];
  tableDef: Array<any> = [
    {
      key: 'id',
      header: 'ID',
    },
    {
      key: 'username',
      header: 'Nazwa uzytkownika',
    },
    {
      key: 'email',
      header: 'Email',
    },
    {
      key: 'role',
      header: 'Typ',
    },
  ];

  params = this.getRequestParams(
    this.page,
    this.pageSize,
    this.sort,
    this.textSearched,
    this.selectedRoleId
  );

  sortFields = [
    { id: 1, itemName: '▲ Nazwa użytkownika', itemArray: ['username', 'asc'] },
    { id: 2, itemName: '▼ Nazwa użytkownika', itemArray: ['username', 'desc'] },
    { id: 3, itemName: '▲ Email', itemArray: ['email', 'asc'] },
    { id: 4, itemName: '▼ Email', itemArray: ['email', 'desc'] },
    { id: 5, itemName: '▲ Typ', itemArray: ['role', 'asc'] },
    { id: 6, itemName: '▼ Typ', itemArray: ['role', 'desc'] },
  ];

  constructor(private userService: UserService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.isLogged = this.userService.isLoggedIn();
    if (this.isLogged)
      this.isUser = this.userService.getUserSession().roles.includes('USER');

    this.getUsers();
    this.getRoles();
  }

  getRequestParams(
    page: number,
    pageSize: number,
    sort: String[],
    textSearched: String,
    role: Number
  ): any {
    let params: any = {};

    if (textSearched) {
      params[`query`] = textSearched;
    }

    if (role) {
      params[`roleId`] = role;
    }

    if (page) {
      params[`page`] = page - 1;
    }

    if (pageSize) {
      params[`size`] = pageSize;
    }

    if (sort) {
      params[`sort`] = sort;
    }

    return params;
  }

  getUsers(): void {
    this.params = this.getRequestParams(
      this.page,
      this.pageSize,
      this.sort,
      this.textSearched,
      this.selectedRoleId
    );
    this.userService.getUsers(this.params).subscribe({
      next: (data) => {
        const { users, totalItems } = data;
        this.users = users!;
        this.count = totalItems!;
      },
      error: (err) => {
        this.content = err;
      },
    });
  }

  getRoles(): void {
    this.userService.getRoles().subscribe({
      next: (data) => {
        this.roles = data;
      },
      error: (err) => {
        this.content = err;
      },
    });
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.getUsers();
  }

  handlePageSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
    this.getUsers();
  }

  changeColumnHeader(column: string): string {
    let header = column;

    this.tableDef.map((el) => {
      if (el.key === column) header = el.header;
    });
    return header;
  }

  deleteUser(id: number): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        movieContent: false,
        userContent: true,
        isWaiting: false,
        isOk: false,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userService.deleteUser(id).subscribe(() => {
          this.reloadPage();
        });
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
    window.location.replace('/users');
  }

  changeRole(user: User): void {
    let updateUser = user;
    let id = Number(updateUser!.id);
    if (updateUser.role!.id == 1) {
      updateUser.role! = { id: 2, name: 'MANAGER' };
    } else if (updateUser.role!.id == 2) {
      updateUser.role! = { id: 1, name: 'USER' };
    }

    this.userService.updateUserRole(id, user).subscribe((result) => {
      if (result) this.isRoleUpdated = true;

      this.openAlertDialog();
    });
  }

  onItemSelect(item: Role): void {
    if (item == undefined) this.selectedRoleId = NaN;
    else this.selectedRoleId = item.id;

    this.getUsers();
  }

  onTypeDeSelect(): void {
    this.selectedRoleId = NaN;
    this.getUsers();
  }

  onSortDeSelect(): void {
    this.sort = [];
    this.getUsers();
  }

  onSortSelect(item: any): void {
    if (item !== undefined) {
      this.sortFields.find((el) => {
        if (el.id === item[0].id) this.sort = el.itemArray;
      });
    } else this.sort = [];
    this.getUsers();
  }

  openAlertDialog(): void {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      data: { isRoleUpdated: this.isRoleUpdated },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.isRoleUpdated = false;
    });
  }
}
