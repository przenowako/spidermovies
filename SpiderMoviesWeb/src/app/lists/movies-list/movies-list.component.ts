import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Favorite } from '../../model/favorite';
import { Movie, Genres } from '../../model/movie';
import { User } from '../../model/user';
import { MovieService } from '../../services/movie.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss'],
})
export class MoviesListComponent implements OnInit {
  content?: string;
  currentUser?: User;
  movies: Movie[] = [];
  genres: Genres[] = [];
  selectedGenres = new FormControl([]);
  favoriteMovies: Movie[] = [];
  favoriteMovie?: Favorite;
  selectedGenre?: number;
  query = '';
  page = 1;
  count = 0;
  pageSize = 8;
  pageSizes = [8, 12, 16, 20];
  sort: String[] = [];
  sortFields = [
    { id: 1, itemName: '▲ Tytuł', itemArray: ['title', 'asc'] },
    { id: 2, itemName: '▼ Tytuł', itemArray: ['title', 'desc'] },
    { id: 3, itemName: '▲ Autor', itemArray: ['author', 'asc'] },
    { id: 4, itemName: '▼ Autor', itemArray: ['author', 'desc'] },
    { id: 5, itemName: '▲ Cena', itemArray: ['price', 'asc'] },
    { id: 6, itemName: '▼ Cena', itemArray: ['price', 'desc'] },
  ];

  params = this.getRequestParams(
    this.page,
    this.pageSize,
    this.sort,
    this.query,
    this.selectedGenre!
  );

  constructor(
    private movieService: MovieService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userService.getUserSession();
    this.getMovies();
    this.getGenres();
    if (this.currentUser?.id) this.getFavoriteMovies();
  }

  getRequestParams(
    page: number,
    pageSize: number,
    sort: String[],
    query: string,
    genre: number
  ): any {
    let params: any = {};

    if (query) {
      params[`query`] = query;
    }

    if (genre) {
      params[`genre`] = genre;
    }

    if (page) {
      params[`page`] = page - 1;
    }

    if (pageSize) {
      params[`size`] = pageSize;
    }

    if (sort) {
      params[`sort`] = sort;
    }

    return params;
  }

  getMovies(): void {
    this.params = this.getRequestParams(
      this.page,
      this.pageSize,
      this.sort,
      this.query,
      this.selectedGenre!
    );
    this.movieService.getMovies(this.params).subscribe({
      next: (data) => {
        const { movies, totalItems } = data;
        this.movies = movies!;
        this.count = totalItems!;
      },
      error: (err) => {
        this.content = err;
      },
    });
  }

  getFavoriteMovies(): void {
    this.userService.getfavoriteMovies(this.currentUser?.id).subscribe({
      next: (data) => {
        const { movies, totalItems } = data;
        this.favoriteMovies = movies!;
      },
    });
  }

  getGenres(): void {
    this.movieService.getGenres().subscribe({
      next: (data) => {
        this.genres = data;
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
            this.content = res.message;
          } catch {
            this.content = `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          this.content = `Error with status: ${err.status}`;
        }
      },
    });
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.getMovies();
  }

  handlePageSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
    this.getMovies();
  }

  onGenreDeSelect() {
    this.selectedGenre = undefined;
    this.getMovies();
  }

  onSortDeSelect() {
    this.sort = [];
    this.getMovies();
  }

  addToFavorite(favoriteMovie: Favorite) {
    this.userService.setFavoriteMovie(favoriteMovie).subscribe({
      next: (data) => {
        this.getFavoriteMovies();
      },
    });
  }

  removeFromFavorite(favoriteMovie: Favorite) {
    this.userService.deleteFavoriteMovie(favoriteMovie).subscribe({
      next: (data) => {
        this.getFavoriteMovies();
      },
    });
  }

  updateFavoriteMovie(id: any) {
    this.favoriteMovie = {
      movieId: id,
      userId: Number(this.currentUser?.id!),
    };
    if (this.isFavorite(id)) this.removeFromFavorite(this.favoriteMovie);
    else this.addToFavorite(this.favoriteMovie);
  }

  isFavorite(id: any): boolean {
    if (this.favoriteMovies.find((el) => el.id === id)) return true;
    return false;
  }
}
