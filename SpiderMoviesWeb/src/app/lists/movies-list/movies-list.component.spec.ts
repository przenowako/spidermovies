import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Genres, Movie } from '../../model/movie';
import { MoviesListComponent } from './movies-list.component';

describe('MoviesListComponent', () => {
  let component: MoviesListComponent;
  let fixture: ComponentFixture<MoviesListComponent>;
  let fakeMovies: Movie[] = [];
  let fakeGenres: Genres[] = [];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoviesListComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MoviesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create movies', () => {
    expect(component.movies).toEqual(fakeMovies);
  });

  it('should create genres', () => {
    expect(component.genres).toEqual(fakeGenres);
  });

  it('should create favoriteMovies', () => {
    expect(component.favoriteMovies).toEqual(fakeMovies);
  });

  it('should create sortFields', () => {
    expect(component.sortFields).toEqual([
      { id: 1, itemName: '▲ Tytuł', itemArray: ['title', 'asc'] },
      { id: 2, itemName: '▼ Tytuł', itemArray: ['title', 'desc'] },
      { id: 3, itemName: '▲ Autor', itemArray: ['author', 'asc'] },
      { id: 4, itemName: '▼ Autor', itemArray: ['author', 'desc'] },
      { id: 5, itemName: '▲ Cena', itemArray: ['price', 'asc'] },
      { id: 6, itemName: '▼ Cena', itemArray: ['price', 'desc'] },
    ]);
  });

  it('should create page', () => {
    expect(component.page).toEqual(1);
  });

  it('should create count', () => {
    expect(component.count).toEqual(0);
  });

  it('should create pageSize', () => {
    expect(component.pageSize).toEqual(8);
  });

  it('should create pageSizes', () => {
    expect(component.pageSizes).toEqual([8, 12, 16, 20]);
  });

  it('should create sort', () => {
    expect(component.sort).toEqual([]);
  });

  it('should create params', () => {
    expect(component.params).toEqual(
      component.getRequestParams(1, 8, [], '', 0)
    );
  });
});
