import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: User = {
    username: '',
    email: '',
    password: '',
    qrCode: null
  };

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  qrCode: number | null = null;

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    if (this.userService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.userService.getUserSession().roles;
    }
  }

  onSubmit(): void {
    const { username, password, qrCode } = this.form;

    this.authService.login(username, password, qrCode!).subscribe({
      next: (data) => {
        this.userService.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.userService.getUserSession().roles;
        this.reloadPage();
      },
      error: (err) => {
        this.errorMessage = err.message;
        this.isLoginFailed = true;
      },
    });
  }

  reloadPage(): void {
    window.location.reload();
    window.location.replace('/');
  }
}
