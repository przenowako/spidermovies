import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { User } from '../model/user';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let fakeForm: User = {
    username: '',
    email: '',
    password: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [HttpClientTestingModule, FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create form', () => {
    expect(component.form).toEqual(fakeForm);
  });

  it('should create isLoggedIn', () => {
    expect(component.isLoggedIn).toBeFalsy();
  });
  it('should create isLoginFailed', () => {
    expect(component.isLoginFailed).toBeFalsy();
  });
  it('should create roles', () => {
    expect(component.roles).toEqual([]);
  });
});
