export interface Genres {
  /**
   * id of genre
   */
  id?: number;
  /**
   * name of genre
   */
  name?: string;
}

export interface Movie {
  /**
   * id of movie
   */
  id?: number;
  /**
   * title of movie
   */
  title?: string;
  /**
   * author of movie
   */
  author?: string;
  /**
   * url to image of movie
   */
  image?: string;
  /**
   * price of movie
   */
  price?: number;
  /**
   * release date of movie
   */
  releaseDate?: Date;
  /**
   * age classification of movie
   */
  ageClassification?: number;
  /**
   * description of movie
   */
  description?: string;
  /**
   * array of movie genres
   */
  genres?: Genres[];
}

export interface MoviesResponse {
  /**
   * movies per page
   */
  movies?: Movie[];
  /**
   * current page
   */
  currentPage?: number;
  /**
   * total items
   */
  totalItems?: number;
  /**
   * total pages
   */
  totalPages?: number;
}
