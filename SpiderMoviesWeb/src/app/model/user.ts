export interface User {
  /**
   * id of user
   */
  id?: number;
  /**
   * user's username
   */
  username: string;
  /**
   * user's email
   */
  email: string;
  /**
   * user's password
   */
  password: string;
  /**
   * user's name
   */
  name?: string;
  /**
   * user's last name
   */
  lastName?: string;
  /**
   * user's address
   */
  address?: string;
  /**
   * user's role
   */
  role?: Role;
  /**
   * user's role
   */
  qrCode?: number | null;
}

export interface UsersResponse {
  /**
   * users per page
   */
  users?: User[];
  /**
   * current page
   */
  currentPage?: number;
  /**
   * total items
   */
  totalItems?: number;
  /**
   * total pages
   */
  totalPages?: number;
}

export interface Role {
  /**
   * id of role
   */
  id: number;
  /**
   * name of role
   */
  name: string;
}

export interface UserPasword {
  /**
   * id of user
   */
  userId: number;
  /**
   * current user password
   */
  currentPassword: string;
  /**
   * new user password
   */
  newPassword: string;
}
