export interface Favorite {
  /**
   * id of user
   */
  userId?: number;
  /**
   * id of movie
   */
  movieId?: number;
}
