-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2023 at 11:35 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spidermovies`
--
DROP DATABASE IF EXISTS `spidermovies`;
CREATE DATABASE IF NOT EXISTS `spidermovies` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `spidermovies`;

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
CREATE TABLE `genres` (
  `id` bigint(20) NOT NULL,
  `name` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Familijny'),
(2, 'Fantasy'),
(3, 'Przygodowy'),
(4, 'Dramat'),
(5, 'Komedia'),
(6, 'Polski'),
(7, 'Animowany'),
(8, 'Muzyczny'),
(9, 'Sci-Fi'),
(10, 'Akcja'),
(11, 'Horror'),
(12, 'Wojenny'),
(13, 'Biograficzny'),
(14, 'Przyrodniczy');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
CREATE TABLE `movies` (
  `id` bigint(20) NOT NULL,
  `age_classification` int(11) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `release_date` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `age_classification`, `author`, `description`, `image`, `price`, `release_date`, `title`) VALUES
(1, 13, 'Chris Columbus', 'W dniu jedenastych urodzin Harry dowiaduje się, że jest czarodziejem. Czeka na niego szkoła magii pełna tajemnic.', 'https://i.imgur.com/6LpqCaH.jpg', 3, '2002-01-18 01:00:00', 'HARRY POTTER I KAMIEŃ FILOZOFICZNY'),
(2, 13, 'Alfonso Cuarón', 'Z więzienia ucieka Syriusz Black, Harry nie może już czuć się bezpiecznie w szkole.', 'https://i.imgur.com/fI9IDwk.jpg', 3.5, '2004-06-04 02:00:00', 'HARRY POTTER I WIĘZIEŃ AZKABANU'),
(3, 16, 'Frank Darabont', 'Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.', 'https://i.imgur.com/O1zDP16.jpg', 2.5, '2000-03-24 01:00:00', 'ZIELONA MILA'),
(4, 16, 'Stanisław Bareja', 'Pełne paradoksów i komizmu życie prezesa klubu sportowego \'Tęcza\' w czasach PRL-u.', 'https://i.imgur.com/UgoEM4b.jpg', 3.28, '1981-05-04 02:00:00', 'MIŚ'),
(5, 6, 'Garth Jennings', 'Aby uratować upadający teatr, miś koala organizuje konkurs w celu znalezienia wokalisty z najpiękniejszym głosem na świecie.', 'https://i.imgur.com/eOyYTco.jpg', 1.5, '2016-12-21 01:00:00', 'SING'),
(6, 6, 'Chris Buck, Jennifer Lee', 'Księżniczka Elsa ucieka ze swojej koronacji w góry, gdzie tworzy lodowy pałac.', 'https://i.imgur.com/1FELYL1.jpg', 2.3, '2013-09-29 02:00:00', 'KRAINA LODU'),
(7, 13, 'Daniel Espinosa', 'Biochemik Michael Morbius, próbując wyleczyć się z rzadkiej choroby krwi, niechcący zaraża się pewnym rodzajem wampiryzmu.', 'https://i.imgur.com/rP8w2kV.jpg', 8.99, '2022-03-30 02:00:00', 'MORBIUS'),
(8, 15, 'Jan Komasa', 'Miłość dwojga młodych ludzi zostaje wystawiona na próbę, gdy 1 sierpnia 1944 wybucha Powstanie Warszawskie.', 'https://i.imgur.com/AaVztgQ.jpg', 6.59, '2014-07-30 02:00:00', 'MIASTO 44'),
(9, 15, 'Michał Węgrzyn', 'Historia Tomasza Chady - rapera, który ginie niespodziewanie w 2018 roku. Śmierć muzyka szokuje wszystkich wokół.', 'https://i.imgur.com/Y81ULpY.jpg', 4.39, '2019-11-15 01:00:00', 'PROCEDER'),
(10, 13, 'Jacques Perrin, Jacques Cluzaud', 'Dokument ukazuje piękno życia w oceanach, oraz zwraca uwagę na zagrażające Ziemi niebezpieczeństwa.', 'https://i.imgur.com/Ak8dm27.jpg', 1.99, '2009-10-13 02:00:00', 'OCEANY');

-- --------------------------------------------------------

--
-- Table structure for table `movie_genre`
--

DROP TABLE IF EXISTS `movie_genre`;
CREATE TABLE `movie_genre` (
  `movie_id` bigint(20) NOT NULL,
  `genre_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `movie_genre`
--

INSERT INTO `movie_genre` (`movie_id`, `genre_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 4),
(4, 5),
(4, 6),
(5, 5),
(5, 7),
(5, 8),
(6, 1),
(6, 3),
(6, 7),
(6, 8),
(7, 9),
(7, 10),
(7, 11),
(8, 4),
(8, 12),
(9, 13),
(10, 14);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'USER'),
(2, 'MANAGER'),
(3, 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `usertotp`
--

DROP TABLE IF EXISTS `usertotp`;
CREATE TABLE `usertotp` (
  `username` varchar(255) NOT NULL,
  `secret_key` varchar(255) DEFAULT NULL,
  `validation_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `usertotp_scratch_codes`
--

DROP TABLE IF EXISTS `usertotp_scratch_codes`;
CREATE TABLE `usertotp_scratch_codes` (
  `usertotp_username` varchar(255) NOT NULL,
  `scratch_codes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_favorite_movie`
--

DROP TABLE IF EXISTS `user_favorite_movie`;
CREATE TABLE `user_favorite_movie` (
  `user_id` bigint(20) NOT NULL,
  `movie_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movie_genre`
--
ALTER TABLE `movie_genre`
  ADD PRIMARY KEY (`movie_id`,`genre_id`),
  ADD KEY `FK3pdaf1ai9eafeypc7qe401l07` (`genre_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  ADD UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`),
  ADD KEY `FKp56c1712k691lhsyewcssf40f` (`role_id`);

--
-- Indexes for table `usertotp`
--
ALTER TABLE `usertotp`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `usertotp_scratch_codes`
--
ALTER TABLE `usertotp_scratch_codes`
  ADD KEY `FKoilpum8rl8i2i1u29ckkvhx16` (`usertotp_username`);

--
-- Indexes for table `user_favorite_movie`
--
ALTER TABLE `user_favorite_movie`
  ADD PRIMARY KEY (`user_id`,`movie_id`),
  ADD KEY `FK3tcus4td4n5gbs9q9maus229b` (`movie_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `movie_genre`
--
ALTER TABLE `movie_genre`
  ADD CONSTRAINT `FK3pdaf1ai9eafeypc7qe401l07` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`),
  ADD CONSTRAINT `FKg7f38h6umffo51no9ywq91438` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FKp56c1712k691lhsyewcssf40f` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `usertotp_scratch_codes`
--
ALTER TABLE `usertotp_scratch_codes`
  ADD CONSTRAINT `FKoilpum8rl8i2i1u29ckkvhx16` FOREIGN KEY (`usertotp_username`) REFERENCES `usertotp` (`username`);

--
-- Constraints for table `user_favorite_movie`
--
ALTER TABLE `user_favorite_movie`
  ADD CONSTRAINT `FK3tcus4td4n5gbs9q9maus229b` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`),
  ADD CONSTRAINT `FKtjq0ch6fm9xs5rex2ud6rve9` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
