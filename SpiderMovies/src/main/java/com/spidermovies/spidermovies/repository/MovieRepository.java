package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Repository interface for managing movies.
 */
public interface MovieRepository extends JpaRepository<Movie, Long> {
    /**
     * Retrieves a movie by its ID.
     *
     * @param id the ID of the movie
     * @return an Optional containing the movie,
     * or an empty Optional if not found
     */
    Optional<Movie> findMovieById(Long id);
    /**
     * Retrieves all movies that contain the given title or author, paginated.
     *
     * @param title    the title to search for
     * @param author   the author to search for
     * @param pageable the pagination information
     * @return a Page of movies matching the title or author criteria
     */
    Page<Movie> findAllByTitleContainingOrAuthorContaining(String title,
                                                           String author,
                                                           Pageable pageable);
    /**
     * Retrieves all movies belonging to a specific genre, paginated.
     *
     * @param genre    the ID of the genre
     * @param pageable the pagination information
     * @return a Page of movies belonging to the specified genre
     */
    @Query(value = "SELECT * FROM movies "
            + "WHERE id IN ("
            + "SELECT DISTINCT id "
            + "FROM movies AS m "
            + "RIGHT JOIN movie_genre AS mg "
            + "ON m.id = mg.movie_id "
            + "WHERE mg.genre_id = :genre)", nativeQuery = true)
    Page<Movie> findByGenre(@Param("genre") Long genre, Pageable pageable);
    /**
     * Retrieves all movies that contain the given title or author
     * and belong to a specific genre, paginated.
     *
     * @param title    the title to search for
     * @param genre    the ID of the genre
     * @param pageable the pagination information
     * @return a Page of movies matching the title or author criteria
     * and belonging to the specified genre
     */
    @Query(value = "SELECT * FROM movies "
            + "WHERE id IN ("
            + "SELECT DISTINCT id "
            + "FROM movies AS m "
            + "RIGHT JOIN movie_genre AS mg "
            + "ON m.id = mg.movie_id "
            + "WHERE mg.genre_id = :genre AND ("
            + "m.title LIKE %:title% OR m.author LIKE %:title%))",
            nativeQuery = true)
    Page<Movie> findAllByTitleContainingOrAuthorContainingAndGenre(
            @Param("title") String title,
            @Param("genre") Long genre, Pageable pageable);
    /**
     * Checks if a movie exists with the given ID.
     *
     * @param id the ID of the movie
     * @return true if a movie with the given ID exists, false otherwise
     */
    boolean existsById(Long id);
    /**
     * Deletes the user favorites associated with a movie.
     *
     * @param id the ID of the movie
     */
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM user_favorite_movie WHERE movie_id = :id",
            nativeQuery = true)
    void deleteFavoritesByMovieId(@Param("id") Long id);
    /**
     * Deletes the movie-genre associations for a movie.
     *
     * @param id the ID of the movie
     */
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM movie_genre WHERE movie_id = :id",
            nativeQuery = true)
    void deleteMovieGenreByMovieId(@Param("id") Long id);
}
