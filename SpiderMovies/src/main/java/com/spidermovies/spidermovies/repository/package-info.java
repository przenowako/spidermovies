/**
 * Provides classes for accessing and manipulating data in the SpiderMovies
 * application.
 * This package contains repository interfaces and classes that define data
 * access operations for various entities in the application, such as movies,
 * users, genres, etc.
 */
package com.spidermovies.spidermovies.repository;
