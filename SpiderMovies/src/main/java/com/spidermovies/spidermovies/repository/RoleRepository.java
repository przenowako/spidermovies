package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.Roles;
import com.spidermovies.spidermovies.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repository interface for managing Role entities in the database.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    /**
     * Retrieves a role by its name.
     *
     * @param name the name of the role
     * @return an Optional containing the role,
     * or an empty Optional if not found
     */
    Optional<Role> findByName(Roles name);
}
