package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Repository interface for managing genres.
 */
public interface GenreRepository extends JpaRepository<Genre, Long> {
    /**
     * Retrieves a genre by its ID.
     *
     * @param id the ID of the genre
     * @return an Optional containing the genre,
     * or an empty Optional if not found
     */
    Optional<Genre> findGenreById(Long id);
    /**
     * Retrieves genres by their IDs.
     *
     * @param postIdsLong a list of genre IDs
     * @return a Set of genres matching the provided IDs
     */
    Set<Genre> findByIdIn(List<Long> postIdsLong);

}
