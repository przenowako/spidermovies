package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Repository interface for managing User entities in the database.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Retrieves a user by its ID.
     *
     * @param id the ID of the user
     * @return an Optional containing the user,
     * or an empty Optional if not found
     */
    Optional<User> findUserById(Long id);
    /**
     * Retrieves a user by its username.
     *
     * @param username the username of the user
     * @return an Optional containing the user,
     * or an empty Optional if not found
     */
    Optional<User> findByUsername(String username);
    /**
     * Checks if a user with the given username exists.
     *
     * @param username the username to check
     * @return true if a user with the given username exists, false otherwise
     */
    Boolean existsByUsername(String username);
    /**
     * Checks if a user with the given email exists.
     *
     * @param email the email to check
     * @return true if a user with the given email exists, false otherwise
     */
    Boolean existsByEmail(String email);
    /**
     * Checks if a user with the given ID exists.
     *
     * @param id the ID to check
     * @return true if a user with the given ID exists, false otherwise
     */
    boolean existsById(Long id);
    /**
     * Retrieves a page of users based on the username
     * or email containing the given search query.
     *
     * @param username the username search query
     * @param email    the email search query
     * @param pageable the pagination information
     * @return a page of users
     */
    Page<User> findAllByUsernameContainingOrEmailContaining(String username,
                                                            String email,
                                                            Pageable pageable);
    /**
     * Retrieves a page of users with the specified role ID.
     *
     * @param roleId   the ID of the role
     * @param pageable the pagination information
     * @return a page of users with the specified role ID
     */
    @Query(value = "SELECT * FROM  users WHERE role_id = :roleId",
            nativeQuery = true)
    Page<User> findByRolesId(@Param("roleId") Integer roleId,
                             Pageable pageable);
    /**
     * Retrieves a page of users with the specified role ID, where the username
     * or email contains the given search query.
     *
     * @param query    the search query
     * @param roleId   the ID of the role
     * @param pageable the pagination information
     * @return a page of users with the specified role ID
     * and matching search query
     */
    @Query(value = "SELECT * FROM users WHERE role_id = :roleId AND ("
           + "email LIKE %:query% OR username LIKE %:query%)",
            nativeQuery = true)
    Page<User> findAllByUsernameOrEmailAndRole(@Param("query") String query,
                                               @Param("roleId") Integer roleId,
                                               Pageable pageable);
    /**
     * Deletes the user's favorite movies by user ID.
     *
     * @param id the ID of the user
     */
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM user_favorite_movie WHERE user_id = :id",
            nativeQuery = true)
    void deleteFavoritesByUserId(@Param("id") Long id);
}
