package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.UserTOTP;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository interface for managing UserTOTP entities in the database.
 */
public interface UserTOTPRepository extends JpaRepository<UserTOTP, String> {
    /**
     * Retrieves all UserTOTP entities with the given username.
     *
     * @param username the username of the UserTOTP entities to retrieve
     * @return a list of UserTOTP entities
     */
    List<UserTOTP> findAllByUsername(String username);
}
