/**
 * Provides classes for handling exceptions in the SpiderMovies application.
 * This package contains custom exception classes and exception handlers
 * for managing and responding to different types of exceptions.
 */
package com.spidermovies.spidermovies.exception;
