package com.spidermovies.spidermovies.exception;

/**
 * Exception class representing a genre not found error.
 */
public class GenreNotFoundException extends RuntimeException {
    /**
     * Constructs a new GenreNotFoundException with the specified error message.
     *
     * @param s the error message
     */
    public GenreNotFoundException(final String s) {
        super(s);
    }
}
