package com.spidermovies.spidermovies.exception;

/**
 * Exception class representing a movie not found error.
 */
public class MovieNotFoundException extends RuntimeException {
    /**
     * Constructs a new MovieNotFoundException with the specified error message.
     *
     * @param s the error message
     */
    public MovieNotFoundException(final String s) {
        super(s);
    }
}
