package com.spidermovies.spidermovies.exception;

/**
 * Exception class representing a user not found error.
 */
public class UserNotFoundException extends RuntimeException {
    /**
     * Constructs a new UserNotFoundException with the specified error message.
     *
     * @param s the error message
     */
    public UserNotFoundException(final String s) {
        super(s);
    }
}
