package com.spidermovies.spidermovies.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * Represents the relationship between a movie and a genre.
 */
@Getter
@Setter
public class MovieGenre implements Serializable {
    /**
     * The ID of the movie.
     */
    @NotNull
    private Long movieId;
    /**
     * The ID of the genre.
     */
    @NotNull
    private Long genreId;
    /**
     * The set of genres associated with the movie.
     */
    private Set<Genre> genres;
}
