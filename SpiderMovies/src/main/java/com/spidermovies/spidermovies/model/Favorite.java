package com.spidermovies.spidermovies.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * Represents a favorite movie for a user.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Favorite implements Serializable {
    /**
     * The ID of the user who added the movie to favorites.
     */
    @NotNull
    private Long userId;
    /**
     * The ID of the movie that is added to favorites.
     */
    @NotNull
    private Long movieId;
    /**
     * The set of favorite movies for the user.
     * Note: This field may not be used directly in the request body,
     * but can be populated in the response.
     */
    private Set<Movie> favorite;
}
