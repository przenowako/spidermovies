package com.spidermovies.spidermovies.model;

/**
 * Utility class containing constant values.
 */
public final class Constants {
    /**
     * Represents the maximum length for a name.
     */
    public static final int MAX_NAME = 120;
    /**
     * Represents the maximum length for a movie description.
     */
    public static final int MAX_DESCRIPTION = 1000;
    /**
     * The pattern used to format and parse dates in the "yyyy-MM-dd" format.
     */
    public static final String DATA_PATTERN = "yyyy-MM-dd";
    /**
     * The maximum length for a role name.
     */
    public static final int MAX_ROLE_NAME = 20;
    /**
     * The maximum length for a password.
     */
    public static final int MAX_PASSWORD = 120;
    /**
     * The minimum length for a password.
     */
    public static final int MIN_PASSWORD = 6;
    /**
     * The maximum length for an email.
     */
    public static final int MAX_EMAIL = 50;
    /**
     * The minimum length for an email.
     */
    public static final int MIN_EMAIL = 5;
    /**
     * The maximum length for a username.
     */
    public static final int MAX_USERNAME = 50;
    /**
     * The minimum length for a username.
     */
    public static final int MIN_USERNAME = 3;
    /**
     * The maximum age in seconds.
     */
    public static final int MAX_AGE_SECONDS = 86400;
    /**
     * The maximum age (in seconds) for CORS preflight response caching.
     */
    public static final int MAX_AGE = 3600;

    /**
     * Size of the QR code image.
     */
    public static final int QR_CODE_SIZE = 200;

    private Constants() {
        throw new AssertionError("Constants class should not be instantiated");
    }
}
