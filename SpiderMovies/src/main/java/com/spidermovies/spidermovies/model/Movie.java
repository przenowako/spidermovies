package com.spidermovies.spidermovies.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.spidermovies.spidermovies.model.Constants.MAX_DESCRIPTION;
import static com.spidermovies.spidermovies.model.Constants.DATA_PATTERN;

/**
 * Represents a movie entity.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "movies")
public class Movie implements Serializable {
    /**
     * The ID of the movie.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    /**
     * The title of the movie.
     */
    private String title;
    /**
     * The author of the movie.
     */
    private String author;
    /**
     * The image URL of the movie.
     */
    private String image;
    /**
     * The price of the movie.
     */
    private Float price;
    /**
     * The release date of the movie.
     */
    @JsonFormat(pattern = DATA_PATTERN)
    private Date releaseDate;
    /**
     * The age classification of the movie.
     */
    private Integer ageClassification;
    /**
     * The description of the movie.
     */
    @Size(max = MAX_DESCRIPTION)
    private String description;
    /**
     * The set of genres associated with the movie.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinTable(name = "movie_genre",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private Set<Genre> genres = new HashSet<>();

}
