/**
 * Provides classes for defining the data models used in the SpiderMovies
 * application.
 * This package contains various model classes that represent different entities
 * and data structures used in the application's domain.
 */
package com.spidermovies.spidermovies.model;
