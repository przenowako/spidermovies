package com.spidermovies.spidermovies.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static com.spidermovies.spidermovies.model.Constants.MAX_USERNAME;
import static com.spidermovies.spidermovies.model.Constants.MIN_USERNAME;
import static com.spidermovies.spidermovies.model.Constants.MAX_EMAIL;
import static com.spidermovies.spidermovies.model.Constants.MIN_EMAIL;
import static com.spidermovies.spidermovies.model.Constants.MAX_PASSWORD;
import static com.spidermovies.spidermovies.model.Constants.MIN_PASSWORD;

/**
 * Represents a User in the system.
 */
@SuppressWarnings("SpellCheckingInspection")
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class User implements Serializable {
    /**
     * The ID of the user.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    /**
     * The username of the user.
     */
    @NotBlank
    @Size(max = MAX_USERNAME, min = MIN_USERNAME)
    private String username;
    /**
     * The email of the user.
     */
    @NotBlank
    @Size(max = MAX_EMAIL, min = MIN_EMAIL)
    @Email
    private String email;
    /**
     * The password of the user.
     */
    @NotBlank
    @Size(max = MAX_PASSWORD, min = MIN_PASSWORD)
    private String password;
    /**
     * The name of the user.
     */
    private String name;
    /**
     * The last name of the user.
     */
    private String lastName;
    /**
     * The address of the user.
     */
    private String address;
    /**
     * The role of the user.
     */
    @ManyToOne(optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;
    /**
     * The set of favorite movies for the user.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinTable(name = "user_favorite_movie",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id"))
    private Set<Movie> favorites = new HashSet<>();
    /**
     * Constructs a new User object with the given username, email,
     * and password.
     *
     * @param usernamep the username of the user
     * @param emailp    the email of the user
     * @param passwordp the password of the user
     */
    public User(final String usernamep, final String emailp,
                final String passwordp) {
        this.username = usernamep;
        this.email = emailp;
        this.password = passwordp;
    }
    /**
     * Constructs a new User object with the given username, email, password,
     * name, last name, and address.
     *
     * @param usernamep  the username of the user
     * @param emailp     the email of the user
     * @param passwordp  the password of the user
     * @param namep      the name of the user
     * @param lastNamep  the last name of the user
     * @param addressp   the address of the user
     */
    public User(final String usernamep, final String emailp,
                final String passwordp, final String namep,
                final String lastNamep, final String addressp) {
        this.username = usernamep;
        this.email = emailp;
        this.password = passwordp;
        this.name = namep;
        this.lastName = lastNamep;
        this.address = addressp;
    }
    /**
     * Removes a movie from the user's favorites.
     *
     * @param movie the movie to be removed
     */
    public void removeFavorite(final Movie movie) {
        this.favorites.remove(movie);
    }
}
