package com.spidermovies.spidermovies.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.spidermovies.spidermovies.model.Constants.MAX_PASSWORD;
import static com.spidermovies.spidermovies.model.Constants.MIN_PASSWORD;

/**
 * Represents the user password change request.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserPassword {
    /**
     * The ID of the user.
     */
    @NotNull
    private Long userId;
    /**
     * The current password of the user.
     */
    @NotBlank
    @Size(max = MAX_PASSWORD, min = MIN_PASSWORD)
    private String currentPassword;
    /**
     * The new password for the user.
     */
    @NotBlank
    @Size(max = MAX_PASSWORD, min = MIN_PASSWORD)
    private String newPassword;
}
