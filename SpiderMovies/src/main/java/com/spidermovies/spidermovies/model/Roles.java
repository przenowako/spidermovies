package com.spidermovies.spidermovies.model;

/**
 * Enum representing the roles of users in the system.
 */
public enum Roles {
    /**
     * User role.
     */
    USER,

    /**
     * Manager role.
     */
    MANAGER,

    /**
     * Administrator role.
     */
    ADMIN
}
