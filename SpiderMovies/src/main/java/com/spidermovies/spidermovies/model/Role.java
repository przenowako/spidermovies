package com.spidermovies.spidermovies.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import static com.spidermovies.spidermovies.model.Constants.MAX_ROLE_NAME;

/**
 * Represents a role entity in the application.
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "roles")
public class Role {
    /**
     * The ID of the role.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * The name of the role.
     */
    @Enumerated(EnumType.STRING)
    @Column(length = MAX_ROLE_NAME)
    private Roles name;
}
