/**
 * Provides classes for managing user authentication and authorization.
 * This package contains controllers, models, and utility classes related
 * to user authentication and authorization in the SpiderMovies application.
 */
package com.spidermovies.spidermovies;


