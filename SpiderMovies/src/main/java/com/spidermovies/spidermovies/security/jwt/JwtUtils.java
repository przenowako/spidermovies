package com.spidermovies.spidermovies.security.jwt;

import com.spidermovies.spidermovies.security.services.UserDetailsImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static com.spidermovies.spidermovies.model.Constants.MAX_AGE_SECONDS;

/**
 * Utility class for handling JWT (JSON Web Token) operations.
 */
@SuppressWarnings("unused")
@Component
@Setter
public class JwtUtils {
    /**
     * The logger for JwtUtils class.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(JwtUtils.class);
    /**
     * The secret key used for JWT token signing and validation.
     */
    @Value("${spiderMovies.app.jwtSecret}")
    private String jwtSecret;
    /**
     * The expiration time in milliseconds for JWT tokens.
     */
    @Value("${spiderMovies.app.jwtExpirationMs}")
    private int jwtExpirationMs;
    /**
     * The name of the JWT cookie.
     */
    @Value("${spiderMovies.app.jwtCookieName}")
    private String jwtCookie;
    /**
     * Retrieves the JWT token from the cookies in the HTTP request.
     *
     * @param request the HTTP request
     * @return the JWT token, or null if not found
     */
    public String getJwtFromCookies(final HttpServletRequest request) {
        Cookie cookie = WebUtils.getCookie(request, jwtCookie);
        if (cookie != null) {
            return cookie.getValue();
        } else {
            return null;
        }
    }
    /**
     * Generates a JWT token as a response cookie based on the user details.
     *
     * @param userPrincipal the user details
     * @return the JWT token as a response cookie
     */
    public ResponseCookie generateJwtCookie(
            final UserDetailsImpl userPrincipal) {
        String jwt = generateTokenFromUsername(userPrincipal.getUsername());
        return ResponseCookie.
                from(jwtCookie, jwt).path("/").
                maxAge(MAX_AGE_SECONDS).httpOnly(true).build();
    }
    /**
     * Generates a clean JWT token as a response cookie with no value.
     *
     * @return the clean JWT token as a response cookie
     */
    public ResponseCookie getCleanJwtCookie() {
        return ResponseCookie.from(jwtCookie, "").path("/").build();
    }
    /**
     * Extracts the username from the JWT token.
     *
     * @param token the JWT token
     * @return the username extracted from the token
     */
    public String getUserNameFromJwtToken(final String token) {
        return Jwts.parser().setSigningKey(jwtSecret).
                parseClaimsJws(token).getBody().getSubject();
    }
    /**
     * Validates the JWT token.
     *
     * @param authToken the JWT token to validate
     * @return true if the token is valid, false otherwise
     */
    public boolean validateJwtToken(final String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException e) {
            LOGGER.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            LOGGER.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            LOGGER.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
    /**
     * Generates a JWT token based on the username.
     *
     * @param username the username to include in the token
     * @return the generated JWT token
     */
    public String generateTokenFromUsername(final String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime()
                        + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
}
