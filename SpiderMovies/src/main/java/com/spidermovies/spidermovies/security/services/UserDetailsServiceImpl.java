package com.spidermovies.spidermovies.security.services;

import com.spidermovies.spidermovies.model.User;
import com.spidermovies.spidermovies.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom implementation of the Spring Security UserDetailsService interface.
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    /**
     * Repository for accessing user data.
     */
    @Autowired
    private UserRepository userRepository;
    /**
     * Loads user-specific data based on the provided username.
     *
     * @param username the username of the user to load
     * @return UserDetailsImpl object representing the user details
     * @throws UsernameNotFoundException if the user is not found
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(
                        "Nie znaleziono użytkownika: " + username));

        return UserDetailsImpl.build(user);
    }
}
