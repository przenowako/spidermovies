/**
 * Provides services for user authentication and authorization
 * in the SpiderMoviesApplication.
 * This package contains classes responsible for user details retrieval,
 * user registration,
 * password encoding, and other user-related operations.
 */
package com.spidermovies.spidermovies.security.services;
