package com.spidermovies.spidermovies.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object for validating a code associated with a username.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidateCodeDto {
    /**
     * The code to be validated.
     */
    private Integer code;
    /**
     * The username associated with the code.
     */
    private String username;
}
