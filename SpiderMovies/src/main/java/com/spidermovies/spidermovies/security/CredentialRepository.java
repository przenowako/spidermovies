package com.spidermovies.spidermovies.security;

import com.spidermovies.spidermovies.model.UserTOTP;
import com.spidermovies.spidermovies.repository.UserTOTPRepository;
import com.warrenstrange.googleauth.ICredentialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repository class for managing user credentials used
 * in two-factor authentication.
 * It implements the ICredentialRepository interface
 * from the GoogleAuth library.
 */
@SuppressWarnings("unused")
@Component
public class CredentialRepository implements ICredentialRepository {

    /**
     * A map to store the user's TOTP (Time-based One-Time Password)
     * information,where the key is the username and the value
     * is the corresponding UserTOTP object.
     */
    private final Map<String, UserTOTP> usersKeys = new HashMap<>();

    /**
     * @Autowired field for UserTOTPRepository;
     */
    @Autowired
    private UserTOTPRepository userTOTPRepository;
    /**
     * Retrieves the secret key associated with the given username.
     *
     * @param userName the username of the user
     * @return the secret key as a string
     */
    @Override
    public String getSecretKey(final String userName) {
        loadUserTOTP(userName);
        try {
            return usersKeys.get(userName).getSecretKey();
        } catch (NullPointerException e) {
            return e.getMessage();
        }
    }
    /**
     * Saves the user's credentials, including the secret key,
     * validation code, and scratch codes.
     *
     * @param userName      the username of the user
     * @param secretKey     the secret key associated with the user
     * @param validationCode the current validation code generated
     *                       by the TOTP algorithm
     * @param scratchCodes   the list of scratch codes for emergency use
     */
    @Override
    public void saveUserCredentials(final String userName,
                                    final String secretKey,
                                    final int validationCode,
                                    final List<Integer> scratchCodes) {
        UserTOTP userTOTP = new UserTOTP(userName, secretKey, validationCode,
                scratchCodes);
        usersKeys.put(userName, userTOTP);
        userTOTPRepository.save(userTOTP);
    }
    /**
     * Retrieves the UserTOTP object for the specified username.
     *
     * @param username the username of the user
     * @return the UserTOTP object associated with the username
     */
    public UserTOTP getUser(final String username) {
        return usersKeys.get(username);
    }
    /**
     * Loads the user's TOTP information from the database and updates
     * the usersKeys map.
     *
     * @param userName the username of the user
     */
    public void loadUserTOTP(final String userName) {
        List<UserTOTP> userTOTPs = userTOTPRepository.
                findAllByUsername(userName);
        usersKeys.clear();
        for (UserTOTP userTOTP : userTOTPs) {
            usersKeys.put(userTOTP.getUsername(), userTOTP);
        }
    }
}
