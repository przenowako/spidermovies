package com.spidermovies.spidermovies.security.jwt;

import com.spidermovies.spidermovies.security.services.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A filter for processing authentication tokens in Spring Security.
 * Extends OncePerRequestFilter to ensure it is invoked only once per request.
 */
@SuppressWarnings("unused")
public class AuthTokenFilter extends OncePerRequestFilter {
    /**
     * The utility class for JWT token handling.
     */
    @Autowired
    private JwtUtils jwtUtils;
    /**
     * The user details service for loading user information.
     */
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    /**
     * The logger instance for logging purposes.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AuthTokenFilter.class);
    /**
     * Performs the authentication token filtering logic.
     *
     * @param request     the HTTP request
     * @param response    the HTTP response
     * @param filterChain the filter chain for invoking subsequent filters
     * @throws ServletException if a servlet error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain)
            throws ServletException, IOException {
        try {
            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                String username = jwtUtils.getUserNameFromJwtToken(jwt);

                UserDetails userDetails =
                        userDetailsService.loadUserByUsername(username);

                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(userDetails,
                                null,
                                userDetails.getAuthorities());

                authentication.setDetails(new WebAuthenticationDetailsSource().
                        buildDetails(request));

                SecurityContextHolder.getContext().
                        setAuthentication(authentication);
            }
        } catch (Exception e) {
            LOGGER.error("Cannot set user authentication: {}", e.getMessage());
        }

        filterChain.doFilter(request, response);
    }
    /**
     * Parses the JWT token from the request.
     *
     * @param request the HTTP request
     * @return the JWT token
     */
    private String parseJwt(final HttpServletRequest request) {
        return jwtUtils.getJwtFromCookies(request);
    }
}
