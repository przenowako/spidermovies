/**
 * Provides classes for handling JSON Web Tokens (JWT) in the SpiderMovies
 * application.
 * This package contains classes responsible for JWT authentication,
 * token generation,
 * token validation, and token filtering.
 */
package com.spidermovies.spidermovies.security.jwt;
