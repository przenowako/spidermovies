package com.spidermovies.spidermovies.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Component that handles unauthorized access to the application's resources.
 * It implements the AuthenticationEntryPoint interface and is responsible
 * for sending the unauthorized response to clients.
 */
@Component
public class AuthEntryPointJwt implements AuthenticationEntryPoint {
    /**
     * Logger for the AuthEntryPointJwt class.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AuthEntryPointJwt.class);
    /**
     * Invoked when a user tries to access a secured resource
     * without proper authentication.
     * It sends an unauthorized response to the client.
     *
     * @param request       the HTTP request
     * @param response      the HTTP response
     * @param authException the AuthenticationException that caused
     *                      the unauthorized access
     * @throws IOException if an I/O error occurs while writing the response
     */
    @Override
    public void commence(final HttpServletRequest request,
                         final HttpServletResponse response,
                         final AuthenticationException authException)
            throws IOException {
        LOGGER.error("Unauthorized error: {}", authException.getMessage());

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        final Map<String, Object> body = new HashMap<>();
        body.put("status", HttpServletResponse.SC_UNAUTHORIZED);
        body.put("error", "Unauthorized");
        body.put("message", authException.getMessage());
        body.put("path", request.getServletPath());

        final ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getOutputStream(), body);
    }
}
