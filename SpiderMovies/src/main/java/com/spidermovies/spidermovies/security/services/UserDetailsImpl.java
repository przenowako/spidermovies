package com.spidermovies.spidermovies.security.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spidermovies.spidermovies.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Custom implementation of the Spring Security UserDetails interface.
 */
@SuppressWarnings("SpellCheckingInspection")
public class UserDetailsImpl implements UserDetails {
    /**
     * The serial version UID for serialization.
     */
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * The user ID.
     */
    private final Long id;
    /**
     * The username.
     */
    private final String username;
    /**
     * The email address.
     */
    private final String email;
    /**
     * The password (ignored during JSON serialization).
     */
    @JsonIgnore
    private final String password;
    /**
     * The authorities/roles assigned to the user.
     */
    private final Collection<? extends GrantedAuthority> authorities;
    /**
     * Constructs a UserDetailsImpl object with the provided user details.
     *
     * @param idp           the user ID
     * @param usernamep     the username
     * @param emailp        the email address
     * @param passwordp     the password (ignored during JSON serialization)
     * @param authoritiesp  the authorities/roles assigned to the user
     */
    public UserDetailsImpl(final Long idp, final String usernamep,
                           final String emailp, final String passwordp,
                           final Collection<? extends GrantedAuthority>
                                   authoritiesp) {
        this.id = idp;
        this.username = usernamep;
        this.email = emailp;
        this.password = passwordp;
        this.authorities = authoritiesp;
    }
    /**
     * Builds a UserDetailsImpl object based on the provided User object.
     *
     * @param user the User object
     * @return UserDetailsImpl object
     */
    public static UserDetailsImpl build(final User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getName()
                .name()));
        return new UserDetailsImpl(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities);
    }
    /**
     * Returns the authorities/roles assigned to the user.
     *
     * @return the collection of granted authorities
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
    /**
     * Retrieves the user ID.
     *
     * @return the user ID
     */
    public Long getId() {
        return id;
    }
    /**
     * Retrieves the user's email address.
     *
     * @return the email address
     */
    public String getEmail() {
        return email;
    }
    /**
     * Returns the user's password.
     *
     * @return the password
     */
    @Override
    public String getPassword() {
        return password;
    }
    /**
     * Returns the user's username.
     *
     * @return the username
     */
    @Override
    public String getUsername() {
        return username;
    }
    /**
     * Checks if the user account is not expired.
     *
     * @return {@code true} if the user account is not expired,
     * {@code false} otherwise
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    /**
     * Checks if the user account is not locked.
     *
     * @return {@code true} if the user account is not locked,
     * {@code false} otherwise
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    /**
     * Checks if the user credentials are not expired.
     *
     * @return {@code true} if the user credentials are not expired,
     * {@code false} otherwise
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    /**
     * Checks if the user is enabled.
     *
     * @return {@code true} if the user is enabled, {@code false} otherwise
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
    /**
     * Compares this UserDetailsImpl object with the specified
     * object for equality.
     * Returns {@code true} if the specified object is also
     * a UserDetailsImpl object
     * and the user IDs are equal; {@code false} otherwise.
     *
     * @param o the object to compare
     * @return {@code true} if the objects are equal, {@code false} otherwise
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }
    /**
     * Returns the hash code value for this UserDetailsImpl object.
     *
     * @return the hash code value for the object
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
