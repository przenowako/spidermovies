/**
 * Provides classes for defining payload objects used in the SpiderMovies
 * application.
 * This package contains various payload classes that represent data transfer
 * objects
 * (DTOs) used for communication between the client and server,
 * including request
 * and response payloads.
 */
package com.spidermovies.spidermovies.payload;
