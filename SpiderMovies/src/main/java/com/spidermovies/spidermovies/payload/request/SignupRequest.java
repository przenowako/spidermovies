package com.spidermovies.spidermovies.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

import static com.spidermovies.spidermovies.model.Constants.MAX_USERNAME;
import static com.spidermovies.spidermovies.model.Constants.MAX_EMAIL;
import static com.spidermovies.spidermovies.model.Constants.MAX_PASSWORD;
import static com.spidermovies.spidermovies.model.Constants.MIN_PASSWORD;

/**
 * Represents the request payload for user signup.
 */
@Getter
@Setter
public class SignupRequest {
    /**
     * The username for the new user.
     */
    @NotBlank
    @Size(max = MAX_USERNAME)
    private String username;
    /**
     * The email address for the new user.
     */
    @NotBlank
    @Size(max = MAX_EMAIL)
    @Email
    private String email;
    /**
     * The set of roles assigned to the new user.
     */
    private Set<String> role;
    /**
     * The password for the new user.
     */
    @NotBlank
    @Size(min = MIN_PASSWORD, max = MAX_PASSWORD)
    private String password;
}
