package com.spidermovies.spidermovies.payload.response;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a response message.
 */
@SuppressWarnings("SpellCheckingInspection")
@Getter
@Setter
public class MessageResponse {
    /**
     * The message string.
     */

    private String message;
    /**
     * Constructs a new MessageResponse object with the given message.
     *
     * @param messagep the response message
     */
    public MessageResponse(final String messagep) {
        this.message = messagep;
    }
}

