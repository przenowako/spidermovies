package com.spidermovies.spidermovies.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Represents the response containing user information.
 */
@SuppressWarnings("SpellCheckingInspection")
@Getter
@Setter
public class UserInfoResponse {
    /**
     * The ID of the user.
     */
    private Long id;
    /**
     * The username of the user.
     */
    private String username;
    /**
     * The email of the user.
     */

    private String email;
    /**
     * The roles assigned to the user.
     */
    private List<String> roles;
    /**
     * Constructs a new UserInfoResponse object with the given user information.
     *
     * @param idp       the ID of the user
     * @param usernamep the username of the user
     * @param emailp    the email of the user
     * @param rolesp    the roles assigned to the user
     */
    public UserInfoResponse(final Long idp, final String usernamep,
                            final String emailp, final List<String> rolesp) {
        this.id = idp;
        this.username = usernamep;
        this.email = emailp;
        this.roles = rolesp;
    }
}
