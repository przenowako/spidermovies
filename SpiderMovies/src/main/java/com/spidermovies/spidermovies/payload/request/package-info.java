/**
 * Provides classes for representing request payloads in the SpiderMovies
 * application.
 * This package contains request models and classes used for data transfer
 * between
 * the client and server endpoints.
 */
package com.spidermovies.spidermovies.payload.request;
