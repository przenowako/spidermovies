package com.spidermovies.spidermovies.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Represents a login request payload.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {
    /**
     * The username provided for login.
     */
    @NotBlank
    private String username;
    /**
     * The password provided for login.
     */
    @NotBlank
    private String password;
    /**
     * The QR code used for authentication (optional).
     */
    private int qrCode;
}
