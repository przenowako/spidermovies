/**
 * Provides classes for representing response payloads in the SpiderMovies
 * application.
 * This package contains response models and classes used for data transfer
 * between
 * the server and client endpoints.
 */
package com.spidermovies.spidermovies.payload.response;
