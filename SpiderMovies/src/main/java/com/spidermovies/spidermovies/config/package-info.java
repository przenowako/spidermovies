/**
 * Provides classes for configuring various aspects of the SpiderMovies
 * application.
 * This package contains configuration classes for security, database, and other
 * application settings.
 */
package com.spidermovies.spidermovies.config;
