package com.spidermovies.spidermovies.config;

import com.spidermovies.spidermovies.security.CredentialRepository;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * The GoogleAuthenticatorConfig class is a configuration class that creates
 * and configures the GoogleAuthenticator bean for two-factor authentication
 * in the application.
 * It uses the CredentialRepository to store and retrieve user credentials.
 */
@SuppressWarnings("unused")
@Configuration
@RequiredArgsConstructor
public class GoogleAuthenticatorConfig {
    /**
     * The credential repository for storing and retrieving user credentials.
     */
    private final CredentialRepository credentialRepository;

    /**
     * Creates and configures the GoogleAuthenticator bean.
     * The GoogleAuthenticator bean is responsible for generating and validating
     * Google Authenticator codes for two-factor authentication.
     *
     * @return The configured GoogleAuthenticator bean.
     */
    @Bean
    public GoogleAuthenticator gAuth() {
        GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
        googleAuthenticator.setCredentialRepository(credentialRepository);
        return googleAuthenticator;
    }
}
