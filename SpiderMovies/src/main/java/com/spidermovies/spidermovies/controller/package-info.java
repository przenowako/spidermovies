/**
 * Provides classes for handling HTTP requests and serving responses
 * in the SpiderMovies application.
 * This package contains controllers responsible for handling various endpoints
 * and implementing the application's business logic.
 */
package com.spidermovies.spidermovies.controller;
