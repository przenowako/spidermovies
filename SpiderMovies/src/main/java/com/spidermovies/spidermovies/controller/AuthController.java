package com.spidermovies.spidermovies.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.spidermovies.spidermovies.model.Roles;
import com.spidermovies.spidermovies.model.User;
import com.spidermovies.spidermovies.model.UserPassword;
import com.spidermovies.spidermovies.model.Role;
import com.spidermovies.spidermovies.payload.request.LoginRequest;
import com.spidermovies.spidermovies.payload.request.SignupRequest;
import com.spidermovies.spidermovies.payload.response.MessageResponse;
import com.spidermovies.spidermovies.payload.response.UserInfoResponse;
import com.spidermovies.spidermovies.repository.RoleRepository;
import com.spidermovies.spidermovies.repository.UserRepository;
import com.spidermovies.spidermovies.security.ValidateCodeDto;
import com.spidermovies.spidermovies.security.jwt.JwtUtils;
import com.spidermovies.spidermovies.security.services.UserDetailsImpl;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import static com.spidermovies.spidermovies.model.Constants.QR_CODE_SIZE;
import static com.spidermovies.spidermovies.model.Constants.MAX_AGE;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The AuthController class is a controller that handles authentication
 *  and user-related requests in the SpiderMovies application.
 *  It provides endpoints for user sign-in, sign-up, password change,
 *  sign-out, QR code generation, and authentication code validation.
 *  The controller uses various autowired dependencies such as
 *  AuthenticationManager, UserRepository, RoleRepository, PasswordEncoder,
 *  JwtUtils, and GoogleAuthenticator for implementing authentication
 *  and authorization features.
 */
@CrossOrigin(origins = "*", maxAge = MAX_AGE)
@RestController
@RequiredArgsConstructor
@RequestMapping("/movies/auth")
@SuppressWarnings({"MissingDefaultConstructorComment", "unused",
        "SpellCheckingInspection"})
public class AuthController {

    /**
     * @Autowired field for AuthenticationManager;
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * @Autowired field for UserRepository;
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * @Autowired field for RoleRepository;
     */
    @Autowired
    private RoleRepository roleRepository;

    /**
     * @Autowired field for PasswordEncoder;
     */
    @Autowired
    private final PasswordEncoder encoder;

    /**
     * @Autowired field for JwtUtils;
     */
    @Autowired
    private final JwtUtils jwtUtils;

    /**
     * @Autowired field for BCryptPasswordEncoder;
     */
    private final BCryptPasswordEncoder passwordEncoder =
            new BCryptPasswordEncoder();

    /**
     * The Google Authenticator instance used for authentication.
     */
    private final GoogleAuthenticator gAuth;

    /**
     * The logger for AuthController class.
     */
    private static final Logger LOGGER =
            LogManager.getLogger(AuthController.class);

    /**
     * Authenticates a user based on the provided login request.
     *
     * @param loginRequest The login request containing the username,
     *                     password, and QR code.
     * @return The response entity containing the user information and
     * a JWT cookie upon successful authentication,
     *         or an unauthorized response if the user is not authorized.
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(
            @Valid @RequestBody final LoginRequest loginRequest) {
        if (gAuth.authorizeUser(
                loginRequest.getUsername(), loginRequest.getQrCode())) {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),
                            loginRequest.getPassword()));

            SecurityContextHolder.getContext().
                    setAuthentication(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl)
                    authentication.getPrincipal();

            ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

            List<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());

            UserInfoResponse userInfoResponse = new UserInfoResponse(
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles
            );

            LOGGER.info("User authenticated successfully: {}",
                    loginRequest.getUsername());

            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                    .body(userInfoResponse);

        } else {
            LOGGER.warn("Failed authentication for user: {}",
                    loginRequest.getUsername());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Changes the password for a user.
     *
     * @param userPassword The user's current and new password information.
     * @return The response entity with an OK status if the password
     * is successfully changed,a forbidden status if the current
     * is incorrect, or a not found status if the user is not found.
     */
    @PutMapping("/updatePassword")
    public ResponseEntity<?> changeUserPassword(
            @RequestBody final UserPassword userPassword) {
        Optional<User> user = userRepository.findUserById(
                userPassword.getUserId());

        if (user.isPresent()) {
            if (passwordEncoder.matches(userPassword.getCurrentPassword(),
                    user.get().getPassword())) {
                user.get().setPassword(
                        encoder.encode(userPassword.getNewPassword()));
                userRepository.save(user.get());
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                LOGGER.warn("Failed to change password for user: {}",
                        user.get().getUsername());
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } else {
            LOGGER.warn("User not found with ID: {}", userPassword.getUserId());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Registers a new user.
     *
     * @param signUpRequest The signup request containing the username,
     *                      email, and password.
     * @return The response entity with a success message if the user
     * is successfully registered,
     *         or a bad request response with an error message
     *         if the username or email is already in use.
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(
            @Valid @RequestBody final SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            LOGGER.warn("Username {} is already in use",
                    signUpRequest.getUsername());
            return ResponseEntity.badRequest().body(
                    new MessageResponse("Nazwa użytkownika jest już używana!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            LOGGER.warn("Email {} is already in use",
                    signUpRequest.getEmail());
            return ResponseEntity.badRequest().body(
                    new MessageResponse("Adres email jest już używany!"));
        }

        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Role role;

        if (user.getUsername().equals("admin")) {
            role = roleRepository.findByName(Roles.ADMIN).orElseThrow(
                    () -> new RuntimeException("Błąd: nie znaleziono roli"));
        } else {
            role = roleRepository.findByName(Roles.USER).orElseThrow(
                    () -> new RuntimeException("Błąd: nie znaleziono roli"));
        }

        user.setRole(role);
        userRepository.save(user);
        LOGGER.info("User registered successfully: {}",
                signUpRequest.getUsername());
        return ResponseEntity.ok(
                new MessageResponse("Użytkownik zarejestrowany pomyślnie!"));
    }

    /**
     * Logs out the user.
     *
     * @param request Request
     * @return The response entity with a clean JWT cookie
     * and a success message indicating successful logout.
     */
    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser(final HttpServletRequest request) {
        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        LOGGER.info("User logged out successfully");
        return ResponseEntity.ok().header(
                HttpHeaders.SET_COOKIE, cookie.toString())
                .body(new MessageResponse("Zostałeś wylogowany!"));
    }

    /**
     * Generates a QR code image for the specified username.
     *
     * @param username The username for which the QR code is generated.
     * @param response The HttpServletResponse used to write the QR code image.
     */
    @SneakyThrows
    @GetMapping(value = "/generate/{username}",
            produces = MediaType.IMAGE_PNG_VALUE)
    public void generate(@PathVariable final String username,
                         final HttpServletResponse response) {
        final GoogleAuthenticatorKey key = gAuth.createCredentials(username);
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        String otpAuthURL = GoogleAuthenticatorQRGenerator.getOtpAuthTotpURL(
                "SpiderMovies", username, key);

        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(
                    otpAuthURL, BarcodeFormat.QR_CODE,
                    QR_CODE_SIZE, QR_CODE_SIZE);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", outputStream);
            byte[] qrCodeBytes = outputStream.toByteArray();

            response.setContentType("image/png");
            ServletOutputStream servletOutputStream =
                    response.getOutputStream();
            servletOutputStream.write(qrCodeBytes);
            servletOutputStream.flush();
            servletOutputStream.close();
            LOGGER.info("QR code generated successfully for usernmae: {}",
                    username);
        } catch (Exception e) {
            LOGGER.error("Error generating QR code", e);
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validates the provided authentication code for the specified user.
     *
     * @param body The ValidateCodeDto containing the usernae
     *             and authentication code.
     * @return true if the authentication code is valid, false otherwise.
     */
    @PostMapping("/validate")
    public boolean validateKey(@RequestBody final ValidateCodeDto body) {
        boolean isValid = gAuth.authorizeUser(body.getUsername(),
                body.getCode());

        if (isValid) {
            LOGGER.info("Authentication code is valid for user: {}",
                    body.getUsername());
        } else {
            LOGGER.warn("Authentication code is invalid for user: {}",
                    body.getUsername());
        }

        return isValid;
    }

}
