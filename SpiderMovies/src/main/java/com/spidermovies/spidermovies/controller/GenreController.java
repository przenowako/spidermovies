package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.Genre;
import com.spidermovies.spidermovies.repository.GenreRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Optional;

/**
 * The GenreController class is a controller that handles requests related
 * to movie genres in the SpiderMovies application.
 * It provides endpoints for retrieving all genres,
 * finding a genre by ID, creating a new genre, updating a genre,
 * and deleting a genre. The controller uses the GenreRepository
 * to interact with the underlying data source.
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/genres")
public class GenreController {

    /**
     * @Autowired field for GenreRepository;
     */
    @Autowired
    private GenreRepository genreRepository;

    /**
     * The logger for GenreController class.
     */
    private static final Logger LOGGER =
            LogManager.getLogger(GenreController.class);

    /**
     * Get all genres.
     *
     * @return ResponseEntity containing a list of genres
     */
    @GetMapping("/all")
    public ResponseEntity<List<Genre>> getAllGenres() {
        List<Genre> genres = genreRepository.findAll();
        LOGGER.info("Fetched {} genres", genres.size());
        return new ResponseEntity<>(genres, HttpStatus.OK);
    }

    /**
     * Get a genre by ID.
     *
     * @param id the ID of the genre
     * @return ResponseEntity containing the genre
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<Genre> getGenreById(@PathVariable("id")
                                                  final Long id) {
        Optional<Genre> optionalGenre = genreRepository.findGenreById(id);
        if (optionalGenre.isPresent()) {
            Genre genre = optionalGenre.get();
            LOGGER.info("Found genre: {}", genre);

            return new ResponseEntity<>(genre, HttpStatus.OK);
        } else {
            LOGGER.warn("Genre not found for id: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create a new genre.
     *
     * @param nameGenre the name of the genre
     * @return ResponseEntity containing the created genre
     */
    @PostMapping("/add")
    public ResponseEntity<Genre> createGenre(@RequestBody
                                                 final String nameGenre) {
        Genre newGenre = new Genre();

        try {
            newGenre.setName(nameGenre);
            genreRepository.save(newGenre);
            LOGGER.info("New genre created: {}", nameGenre);
            return new ResponseEntity<>(newGenre, HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("Failed to create genre", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update a genre by ID.
     *
     * @param id    the ID of the genre to update
     * @param genre the updated genre object
     * @return ResponseEntity containing the updated genre
     */
    @PutMapping("/{id}")
    public ResponseEntity<Genre> updateGenre(@PathVariable("id") final long id,
                                             @RequestBody final Genre genre) {
        Optional<Genre> genreData = genreRepository.findGenreById(id);

        if (genreData.isPresent()) {
            Genre newGenre = genreData.get();
            newGenre.setName(genre.getName());
            try {
                Genre updatedGenre = genreRepository.save(newGenre);
                LOGGER.info("Genre updated: {}", updatedGenre);
                return new ResponseEntity<>(updatedGenre, HttpStatus.OK);
            } catch (Exception e) {
                LOGGER.error("Failed to update genre", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            LOGGER.warn("Genre not found for id: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete a genre by ID.
     *
     * @param id the ID of the genre to delete
     * @return ResponseEntity with the appropriate HTTP status
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteGenre(@PathVariable("id")
                                                      final long id) {
        Optional<Genre> movieGenre = genreRepository.findById(id);
        try {
            genreRepository.deleteById(id);
            LOGGER.info("Genre deleted with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Failed to delete genre with ID: {}", id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
