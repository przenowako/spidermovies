package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.Genre;
import com.spidermovies.spidermovies.model.Movie;
import com.spidermovies.spidermovies.model.User;
import com.spidermovies.spidermovies.repository.GenreRepository;
import com.spidermovies.spidermovies.repository.MovieRepository;
import com.spidermovies.spidermovies.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.DeleteMapping;

import static com.spidermovies.spidermovies.model.Constants.MAX_AGE;

import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Arrays;
import java.util.Set;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * The MovieController class is a controller that handles requests related
 * to movies in the SpiderMovies application. It provides endpoints
 * for retrieving paginated lists of movies, finding a movie by ID,
 * creating a new movie, updating a movie, and deleting a movie.
 * The controller uses the MovieRepository, GenreRepository,
 * and UserRepository to interact with the underlying data source.
 * It also includes Cross-Origin Resource Sharing (CORS) configuration
 * to allow requests from all origins. The class contains various endpoints
 * for different operations on movies, including pagination, searching,
 * filtering and sorting. The controller provides appropriate HTTP response
 * status codes and response entities for successful and failed requests.
 * The response entities include the requested data (movies, movie, or metadata)
 * along with the HTTP status. The class also includes utility methods
 * for sorting and retrieving the sort direction.
 */
@SuppressWarnings("unused")
@CrossOrigin(origins = "*", maxAge = MAX_AGE)
@RestController
@RequestMapping("/movies")
public class MovieController {

    /**
     * @Autowired field for MovieRepository;
     */
    @Autowired
    private MovieRepository movieRepository;

    /**
     * @Autowired field for GenreRepository;
     */
    @Autowired
    private GenreRepository genreRepository;

    /**
     * @Autowired field for UserRepository;
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * The logger for MovieController class.
     */
    private static final Logger LOGGER =
            LogManager.getLogger(MovieController.class);

    /**
     * Returns the sort direction based on the provided direction string.
     *
     * @param direction The sort direction string
     *                  ("asc" for ascending, "desc" for descending)
     * @return The corresponding Sort.Direction value
     */
    private Sort.Direction getSortDirection(final String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }

    /**
     * Retrieves a paginated list of movies
     * based on the provided query parameters.
     *
     * @param query The search query string
     * @param genre The genre ID to filter the movies by
     * @param page The page number (default: 0)
     * @param size The page size (default: 3)
     * @param sort The sorting criteria as an array of strings
     *             in the format "field,direction" (default: "id,asc")
     * @return A ResponseEntity containing the paginated list of movies
     * and additional metadata
     */
    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getAllMoviesPage(
            @RequestParam(required = false) final String query,
            @RequestParam(required = false) final Long genre,
            @RequestParam(defaultValue = "0") final int page,
            @RequestParam(defaultValue = "3") final int size,
            @RequestParam(defaultValue = "id,asc") final String[] sort) {
        try {
            List<Sort.Order> orders = new ArrayList<>();

            if (sort[0].contains(",")) {
                for (String sortOrder : sort) {
                    String[] newSort = sortOrder.split(",");
                    orders.add(new Sort.Order(getSortDirection(
                            newSort[1]), newSort[0]));
                }
            } else {
                orders.add(new Sort.Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Movie> movies;
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
            Page<Movie> pageTuts;

            if (query == null && genre == null) {
                pageTuts = movieRepository.findAll(pagingSort);
            } else if (genre == null) {
                pageTuts = movieRepository
                        .findAllByTitleContainingOrAuthorContaining(
                                query, query, pagingSort);
            } else if (query == null) {
                pageTuts = movieRepository
                        .findByGenre(genre, pagingSort);
            } else {
                pageTuts = movieRepository
                        .findAllByTitleContainingOrAuthorContainingAndGenre(
                                query, genre, pagingSort);
            }

            movies = pageTuts.getContent();
            Map<String, Object> response = new HashMap<>();
            response.put("movies", movies);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            LOGGER.info("Fetched movies with query: {}, genre: {}, page: {}, "
                           + "size: {}, sort: {}",
                    query, genre, page, size, Arrays.toString(sort));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Failed to fetch movies", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Retrieves a movie by its ID,
     * optionally checking if it belongs to a specific user.
     *
     * @param id The ID of the movie
     * @param userId (optional) The ID of the user to check ownership
     * @return A ResponseEntity containing the movie if found,
     * or NOT_FOUND status if not found
     */
    @GetMapping("/user/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable("id") final long id,
                                              @RequestParam(value = "userid",
                                                      required = false)
                                              final long userId) {
        Optional<Movie> movieData = movieRepository.findById(id);
        Optional<User> userData = userRepository.findById(userId);

        if (movieData.isPresent()) {
            Movie movie = movieData.get();
            LOGGER.info("Fetched movie by ID: {}", id);

            if (userData.isPresent()) {
                LOGGER.info("Fetched user by ID: {}", userId);
                return new ResponseEntity<>(movie, HttpStatus.OK);
            } else {
                LOGGER.warn("User not found with ID: {}", userId);
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            LOGGER.warn("Movie not found with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Retrieves a movie by its ID.
     *
     * @param id The ID of the movie
     * @return A ResponseEntity containing the movie if found,
     * or NOT_FOUND status if not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieUser(@PathVariable("id")
                                                  final long id) {
        Optional<Movie> movieData = movieRepository.findById(id);

        if (movieData.isPresent()) {
            Movie movie = movieData.get();
            LOGGER.info("Fetched movie by ID: {}", id);
            return new ResponseEntity<>(movie, HttpStatus.OK);
        } else {
            LOGGER.warn("Movie not found with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Creates a new movie.
     *
     * @param movie The movie object to be created
     * @return A ResponseEntity containing the created movie if successful,
     * or INTERNAL_SERVER_ERROR status if an error occurs
     */
    @PostMapping("/add")
    public ResponseEntity<Movie> createMovie(@RequestBody final Movie movie) {
        try {
            List<Long> genresIds = movie.getGenres().stream().map(Genre::getId).
                    collect(Collectors.toList());
            Set<Genre> genresList = genreRepository.findByIdIn(genresIds);
            movie.setGenres(genresList);
            movieRepository.save(movie);
            LOGGER.info("Created movie with ID: {}", movie.getId());
            return new ResponseEntity<>(movie, HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error("Failed to create movie", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Updates a movie with the specified ID.
     *
     * @param id    The ID of the movie to be updated
     * @param movie The updated movie object
     * @return A ResponseEntity containing the updated movie if found,
     * or NOT_FOUND status if the movie is not found
     */
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable("id") final long id,
                                             @RequestBody final Movie movie) {
        Optional<Movie> movieData = movieRepository.findById(id);

        if (movieData.isPresent()) {
            Movie updateMovie = movieData.get();
            updateMovie.setTitle(movie.getTitle());
            updateMovie.setAuthor(movie.getAuthor());
            updateMovie.setImage(movie.getImage());
            updateMovie.setPrice(movie.getPrice());
            updateMovie.setReleaseDate(movie.getReleaseDate());
            updateMovie.setAgeClassification(movie.getAgeClassification());
            updateMovie.setDescription(movie.getDescription());
            updateMovie.setGenres(movie.getGenres());
            LOGGER.info("Updated movie with ID: {}", id);
            return new ResponseEntity<>(movieRepository.save(updateMovie),
                    HttpStatus.OK);
        } else {
            LOGGER.warn("Movie not found with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes a movie with the specified ID.
     *
     * @param id The ID of the movie to be deleted
     * @return A ResponseEntity with NO_CONTENT status if the movie is
     * successfully deleted, or INTERNAL_SERVER_ERROR status if an error occurs
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteMovie(@PathVariable("id")
                                                      final long id) {
        Optional<Movie> movieData = movieRepository.findById(id);
        try {
            movieRepository.deleteFavoritesByMovieId(id);
            movieRepository.deleteMovieGenreByMovieId(id);
            movieRepository.deleteById(id);
            LOGGER.info("Deleted movie with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            LOGGER.error("Failed to delete movie", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
