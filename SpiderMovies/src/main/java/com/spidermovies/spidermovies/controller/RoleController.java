package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.Role;
import com.spidermovies.spidermovies.repository.RoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The RoleController class is a controller that handles requests related
 * to roles in the SpiderMovies application.
 * It is responsible for retrieving all roles from the RoleRepository
 * and returning them as a ResponseEntity.
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/roles")
public class RoleController {

    /**
     * @Autowired field for RoleRepository;
     */
    @Autowired
    private RoleRepository roleRepository;

    /**
     * The logger for RoleController class.
     */
    private static final Logger LOGGER =
            LogManager.getLogger(RoleController.class);

    /**
     * Retrieves all roles.
     *
     * @return A ResponseEntity containing a list of roles with OK status
     */
    @GetMapping("")
    public ResponseEntity<List<Role>> getRoles() {
        List<Role> roleData = roleRepository.findAll();
        LOGGER.info("Retrieved all roles");
        return new ResponseEntity<>(roleData, HttpStatus.OK);
    }

}
