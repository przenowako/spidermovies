package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.User;
import com.spidermovies.spidermovies.model.Movie;
import com.spidermovies.spidermovies.model.Favorite;
import com.spidermovies.spidermovies.repository.MovieRepository;
import com.spidermovies.spidermovies.repository.UserRepository;
import com.spidermovies.spidermovies.repository.UserTOTPRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Set;

/**
 * Controller class for managing user-related operations.
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/users")
public class UserController {

    /**
     * @Autowired field for UserRepository;
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * @Autowired field for MovieRepository;
     */
    @Autowired
    private MovieRepository movieRepository;

    /**
     * @Autowired field for UserTOTPRepository;
     */
    @Autowired
    private UserTOTPRepository userTOTPRepository;

    /**
     * @Autowired field for PasswordEncoder;
     */
    @Autowired
    private PasswordEncoder encoder;

    /**
     * The logger for UserController class.
     */
    private static final Logger LOGGER =
            LogManager.getLogger(UserController.class);

    /**
     * Determines the sort direction based on the given direction string.
     *
     * @param direction The direction string ("asc" for ascending,
     *                  "desc" for descending)
     * @return The corresponding Sort.Direction value
     */
    private Sort.Direction getSortDirection(final String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }

    /**
     * Retrieve a paginated list of users based on the provided parameters.
     *
     * @param query   The search query string (optional)
     * @param roleId  The ID of the role to filter by (optional)
     * @param page    The page number (default: 0)
     * @param size    The page size (default: 3)
     * @param sort    The sorting criteria in the format
     *                "field,direction" (default: "id,asc")
     * @return A ResponseEntity containing a map with the paginated user data
     */
    @GetMapping("")
    public ResponseEntity<Map<String, Object>> getAllUsersPage(
            @RequestParam(required = false) final String query,
            @RequestParam(required = false) final Integer roleId,
            @RequestParam(defaultValue = "0") final int page,
            @RequestParam(defaultValue = "3") final int size,
            @RequestParam(defaultValue = "id,asc") final String[] sort) {
        try {
            List<Sort.Order> orders = new ArrayList<>();

            if (sort[0].contains(",")) {
                for (String sortOrder : sort) {
                    String[] newSort = sortOrder.split(",");
                    orders.add(new Sort.Order(getSortDirection(
                            newSort[1]), newSort[0]));
                }
            } else {
                orders.add(new Sort.Order(getSortDirection(sort[1]), sort[0]));
            }
            List<User> users;
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<User> pageTuts;

            if (query == null && roleId == null) {
                pageTuts = userRepository.findAll(pagingSort);
            } else if (roleId == null) {
                pageTuts = userRepository.
                        findAllByUsernameContainingOrEmailContaining(
                                query, query, pagingSort);
            } else if (query == null) {
                pageTuts = userRepository.findByRolesId(roleId, pagingSort);
            } else {
                pageTuts = userRepository.findAllByUsernameOrEmailAndRole(
                        query, roleId, pagingSort);
            }

            users = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("users", users);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            LOGGER.info("Retrieved all users");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Failed to retrieve users", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Retrieve a user by its ID.
     *
     * @param id The ID of the user to retrieve
     * @return A ResponseEntity containing the user data if found,
     * or HttpStatus.NOT_FOUND if not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") final long id) {
        Optional<User> userData = userRepository.findById(id);

        if (userData.isPresent()) {
            User user = userData.get();
            user.setPassword(null);
            user.setRole(null);
            user.setFavorites(null);
            LOGGER.info("Retrieved user with ID: {}", id);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            LOGGER.warn("User not found with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update a user by its ID.
     *
     * @param id   The ID of the user to update
     * @param user The updated user data
     * @return A ResponseEntity containing the updated user data if found,
     * or HttpStatus.NOT_FOUND if not found
     */
    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") final long id,
                                           @RequestBody final User user) {
        Optional<User> userData = userRepository.findById(id);

        if (userData.isPresent()) {
            User updateUser = userData.get();
            updateUser.setEmail(user.getEmail());
            updateUser.setName(user.getName());
            updateUser.setLastName(user.getLastName());
            updateUser.setAddress(user.getAddress());

            LOGGER.info("Updated user with ID: {}", id);
            return new ResponseEntity<>(userRepository.save(updateUser),
                    HttpStatus.OK);
        } else {
            LOGGER.warn("User not found with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete a user by its ID.
     *
     * @param id The ID of the user to delete
     * @return A ResponseEntity with HttpStatus.NO_CONTENT
     * if the user is successfully deleted,
     * or HttpStatus.INTERNAL_SERVER_ERROR if an error occurs
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id")
                                                     final long id) {
        try {
            Optional<User> userOptional = userRepository.findUserById(id);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                userRepository.deleteFavoritesByUserId(id);
                userTOTPRepository.deleteById(user.getUsername());
                userRepository.deleteById(id);

                LOGGER.info("Deleted user with ID: {}", id);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                LOGGER.warn("User not found with ID: {}", id);
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error("Error occurred while deleting user with ID: {}",
                    id, e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the role of a user by its ID.
     *
     * @param id   The ID of the user to update
     * @param user The updated user object containing the new role
     * @return A ResponseEntity with the updated user if the operation is
     * successful, or HttpStatus.NOT_FOUND if the user is not found
     */
    @PostMapping("/setRole/{id}")
    public ResponseEntity<User> updateUserRole(
            @PathVariable("id") final long id, @RequestBody final User user) {

        Optional<User> userData = userRepository.findById(id);

        if (userData.isPresent()) {
            User newUser = userData.get();
            newUser.setRole(user.getRole());

            LOGGER.info("Updated role for user with ID: {}", id);
            return new ResponseEntity<>(userRepository.save(newUser),
                    HttpStatus.OK);
        } else {
            LOGGER.warn("User not found with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Add a movie to the favorite movies list of a user.
     *
     * @param favorite The favorite object containing the user ID and movie ID
     * @return A ResponseEntity with the updated user if the operation is
     * successful, or HttpStatus.NOT_FOUND if the user or movie is not found
     */
    @PostMapping("/favoriteMovies")
    public ResponseEntity<?> setFavoriteMovie(
            @RequestBody final Favorite favorite) {
        try {
            Optional<User> userData = userRepository.findById(
                    favorite.getUserId());
            Optional<Movie> userMovie = movieRepository.findMovieById(
                    favorite.getMovieId());

            if (userData.isPresent() && userMovie.isPresent()) {
                Set<Movie> favoriteMovie = userData.get().getFavorites();
                favoriteMovie.add(userMovie.get());
                User newUser = userData.get();
                newUser.setFavorites(favoriteMovie);

                LOGGER.info("Favorite movie added for user with ID: {}",
                        newUser.getId());
                return new ResponseEntity<>(userRepository.save(newUser),
                        HttpStatus.OK);
            } else {
                LOGGER.warn("User or movie not found for setFavoriteMovie");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error("An error occurred while processing setFavoriteMovie",
                    e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove a movie from the favorite movies list of a user.
     *
     * @param favorite The favorite object containing the user ID and movie ID
     * @return A ResponseEntity with the updated user if the operation is
     * successful, or HttpStatus.NOT_FOUND if the user or movie is not found
     */
    @PutMapping("/favoriteMovies")
    public ResponseEntity<?> deleteFavoriteMovie(
            @RequestBody final Favorite favorite) {
        try {
            Optional<User> userData = userRepository.findById(
                favorite.getUserId());
            Optional<Movie> userMovie = movieRepository.findMovieById(
                favorite.getMovieId());

            if (userData.isPresent() && userMovie.isPresent()) {
                User newUser = userData.get();
                newUser.removeFavorite(userMovie.get());

                LOGGER.info("Favorite movie removed for user with ID: {}",
                        newUser.getId());
                return new ResponseEntity<>(userRepository.save(newUser),
                        HttpStatus.OK);
            } else {
                LOGGER.warn("User or movie not found for deleteFavoriteMovie");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
    } catch (Exception e) {
        LOGGER.error("An error occurred while processing "
               + "deleteFavoriteMovie", e);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

    /**
     * Get a paginated list of favorite movies for a specific user.
     *
     * @param id   The ID of the user
     * @param page The page number (default value is 0)
     * @param size The number of items per page (default value is 3)
     * @param sort The sorting criteria in the format "property,direction"
     *             (default value is "id,asc")
     * @return A ResponseEntity with the paginated list of favorite movies
     * if the user is found, or HttpStatus.NOT_FOUND if the user is not found
     */
    @GetMapping("/favoriteMovies/{id}")
    public ResponseEntity<Map<String, Object>> getFavoriteMoviesPage(
            @PathVariable("id") final long id,
            @RequestParam(defaultValue = "0") final int page,
            @RequestParam(defaultValue = "3") final int size,
            @RequestParam(defaultValue = "id,asc") final String[] sort) {
        try {
            Optional<User> userData = userRepository.findById(id);
            Set<Movie> favoriteMovies;

            if (userData.isPresent()) {
                favoriteMovies = userData.get().getFavorites();
            } else {
                LOGGER.warn("User not found for getFavoriteMoviesPage");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            List<Sort.Order> orders = new ArrayList<>();

            if (sort[0].contains(",")) {
                for (String sortOrder : sort) {
                    String[] newSort = sortOrder.split(",");
                    orders.add(new Sort.Order(getSortDirection(newSort[1]),
                            newSort[0]));
                }
            } else {
                orders.add(new Sort.Order(getSortDirection(sort[1]), sort[0]));
            }

            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
            List<Movie> favoriteMoviesList = new ArrayList<>(favoriteMovies);
            Page<Movie> pageTuts = new PageImpl<>(favoriteMoviesList,
                    pagingSort, favoriteMoviesList.size());
            Map<String, Object> response = new HashMap<>();

            response.put("movies", pageTuts.getContent());
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            LOGGER.info("Favorite movies retrieved for user with ID: {}", id);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("An error occurred while processing "
                    + "getFavoriteMoviesPage", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
