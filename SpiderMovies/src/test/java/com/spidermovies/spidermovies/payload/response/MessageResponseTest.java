package com.spidermovies.spidermovies.payload.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageResponseTest {

    @Test
    void testMessageResponse_GetMessage_ReturnsCorrectMessage() {
        String message = "Test message";
        MessageResponse response = new MessageResponse(message);

        assertEquals(message, response.getMessage());
    }

    @Test
    void testMessageResponse_SetMessage_ModifiesMessage() {
        String message = "Test message";
        MessageResponse response = new MessageResponse("");

        response.setMessage(message);

        assertEquals(message, response.getMessage());
    }
}