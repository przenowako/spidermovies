package com.spidermovies.spidermovies.payload.request;

import com.spidermovies.spidermovies.model.Roles;
import com.spidermovies.spidermovies.model.UserPassword;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SignupRequestTest {

    @Test
    public void testGettersAndSetters() {
        SignupRequest signupRequest = new SignupRequest();

        String username = "testUser";
        String email = "test@example.com";
        Set<String> roles = new HashSet<>();
        roles.add(Roles.USER.name());
        String password = "testPassword";

        signupRequest.setUsername(username);
        signupRequest.setEmail(email);
        signupRequest.setRole(roles);
        signupRequest.setPassword(password);

        assertEquals(username, signupRequest.getUsername());
        assertEquals(email, signupRequest.getEmail());
        assertEquals(roles, signupRequest.getRole());
        assertEquals(password, signupRequest.getPassword());
    }

    @Test
    void testSignupRequest_WithValidFields_NoValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("testUser");
        signupRequest.setEmail("test@example.com");
        signupRequest.setPassword("testPassword");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertTrue(violations.isEmpty());
    }

    @Test
    void testSignupRequest_WithBlankUsername_ValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("");
        signupRequest.setEmail("test@example.com");
        signupRequest.setPassword("testPassword");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<SignupRequest> violation = violations.iterator().next();
        assertEquals("username", violation.getPropertyPath().toString());
        assertEquals("nie może być odstępem", violation.getMessage());
    }

    @Test
    void testSignupRequest_WithBlankEmail_ValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("testUser");
        signupRequest.setEmail("");
        signupRequest.setPassword("testPassword");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<SignupRequest> violation = violations.iterator().next();
        assertEquals("email", violation.getPropertyPath().toString());
        assertEquals("nie może być odstępem", violation.getMessage());
    }

    @Test
    void testSignupRequest_WithInvalidEmailFormat_ValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("testUser");
        signupRequest.setEmail("invalidEmail");
        signupRequest.setPassword("testPassword");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<SignupRequest> violation = violations.iterator().next();
        assertEquals("email", violation.getPropertyPath().toString());
        assertEquals("musi być poprawnie sformatowanym adresem e-mail", violation.getMessage());
    }

    @Test
    void testSignupRequest_WithBlankPassword_ValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("testUser");
        signupRequest.setEmail("test@example.com");
        signupRequest.setPassword("");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertEquals(2, violations.size());

        for (ConstraintViolation<SignupRequest> violation : violations) {
            String errorMessage = violation.getMessage();
            assertTrue(errorMessage.equals("nie może być odstępem") ||
                    errorMessage.equals("wielkość musi należeć do zakresu od 6 do 120"));
        }
    }

    @Test
    void testSignupRequest_WithShortPassword_ValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("testUser");
        signupRequest.setEmail("test@example.com");
        signupRequest.setPassword("123");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<SignupRequest> violation = violations.iterator().next();
        assertEquals("password", violation.getPropertyPath().toString());
        assertEquals("wielkość musi należeć do zakresu od 6 do 120", violation.getMessage());
    }

    @Test
    void testSignupRequest_WithLongPassword_ValidationErrors() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("testUser");
        signupRequest.setEmail("test@example.com");
        signupRequest.setPassword("verylongpasswordthatexceedsthemaximumlengthv" +
                "erylongpasswordthatexceedsthemaximumlengthverylongpasswordthat" +
                "exceedsthemaximumlength");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SignupRequest>> violations = validator.validate(signupRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<SignupRequest> violation = violations.iterator().next();
        assertEquals("password", violation.getPropertyPath().toString());
        assertEquals("wielkość musi należeć do zakresu od 6 do 120", violation.getMessage());
    }
}