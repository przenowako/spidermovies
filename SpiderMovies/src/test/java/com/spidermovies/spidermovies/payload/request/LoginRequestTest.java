package com.spidermovies.spidermovies.payload.request;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginRequestTest {

    @Test
    public void testGetAndSetUsername() {
        String username = "testUser";
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        assertEquals(username, loginRequest.getUsername());
    }

    @Test
    public void testGetAndSetPassword() {
        String password = "testPassword";
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPassword(password);
        assertEquals(password, loginRequest.getPassword());
    }

    @Test
    public void testGetAndSetQrCode() {
        int qrCode = 123;
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setQrCode(qrCode);
        assertEquals(qrCode, loginRequest.getQrCode());
    }

    @Test
    void testLoginRequest_WithValidFields_NoValidationErrors() {
        LoginRequest loginRequest = new LoginRequest("testUser", "testPassword", 1234);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<LoginRequest>> violations = validator.validate(loginRequest);

        assertTrue(violations.isEmpty());
    }

    @Test
    void testLoginRequest_WithBlankUsername_ValidationErrors() {
        LoginRequest loginRequest = new LoginRequest("", "testPassword", 1234);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<LoginRequest>> violations = validator.validate(loginRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<LoginRequest> violation = violations.iterator().next();
        assertEquals("username", violation.getPropertyPath().toString());
        assertEquals("nie może być odstępem", violation.getMessage());
    }

    @Test
    void testLoginRequest_WithBlankPassword_ValidationErrors() {
        LoginRequest loginRequest = new LoginRequest("testUser", "", 1234);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<LoginRequest>> violations = validator.validate(loginRequest);

        assertEquals(1, violations.size());
        ConstraintViolation<LoginRequest> violation = violations.iterator().next();
        assertEquals("password", violation.getPropertyPath().toString());
        assertEquals("nie może być odstępem", violation.getMessage());
    }

    @Test
    void testLoginRequest_WithInvalidQrCode_NoValidationErrors() {
        LoginRequest loginRequest = new LoginRequest("testUser", "testPassword", -1234);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<LoginRequest>> violations = validator.validate(loginRequest);

        assertTrue(violations.isEmpty());
    }
}