package com.spidermovies.spidermovies.payload.response;

import com.spidermovies.spidermovies.model.Roles;
import com.spidermovies.spidermovies.payload.response.UserInfoResponse;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserInfoResponseTest {

    @Test
    void testUserInfoResponse_Getters_ReturnsCorrectValues() {
        Long id = 1L;
        String username = "testUser";
        String email = "test@example.com";
        List<String> roles = Arrays.asList(Roles.USER.name(), Roles.MANAGER.name(),
                Roles.ADMIN.name());

        UserInfoResponse response = new UserInfoResponse(id, username, email, roles);

        assertEquals(id, response.getId());
        assertEquals(username, response.getUsername());
        assertEquals(email, response.getEmail());
        assertEquals(roles, response.getRoles());
    }

    @Test
    void testUserInfoResponse_Setters_ModifyValues() {
        Long id = 1L;
        String username = "testUser";
        String email = "test@example.com";
        List<String> roles = Arrays.asList(Roles.USER.name(), Roles.MANAGER.name(),
                Roles.ADMIN.name());

        UserInfoResponse response = new UserInfoResponse(null, "", "", null);

        response.setId(id);
        response.setUsername(username);
        response.setEmail(email);
        response.setRoles(roles);

        assertEquals(id, response.getId());
        assertEquals(username, response.getUsername());
        assertEquals(email, response.getEmail());
        assertEquals(roles, response.getRoles());
    }
}