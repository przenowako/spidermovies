package com.spidermovies.spidermovies.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ValidateCodeDtoTest {

    @Test
    public void testConstructorAndGetters() {
        Integer code = 1234;
        String username = "testuser";

        ValidateCodeDto validateCodeDto = new ValidateCodeDto(code, username);

        Assertions.assertEquals(code, validateCodeDto.getCode());
        Assertions.assertEquals(username, validateCodeDto.getUsername());
    }

    @Test
    public void testSetters() {
        Integer code = 1234;
        String username = "testuser";

        ValidateCodeDto validateCodeDto = new ValidateCodeDto();
        validateCodeDto.setCode(code);
        validateCodeDto.setUsername(username);

        Assertions.assertEquals(code, validateCodeDto.getCode());
        Assertions.assertEquals(username, validateCodeDto.getUsername());
    }
}