package com.spidermovies.spidermovies.security;

import com.spidermovies.spidermovies.model.UserTOTP;
import com.spidermovies.spidermovies.repository.UserTOTPRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CredentialRepositoryTest {

    private CredentialRepository credentialRepository = new CredentialRepository();

    @Mock
    private UserTOTPRepository userTOTPRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        credentialRepository = new CredentialRepository();
        ReflectionTestUtils.setField(credentialRepository, "userTOTPRepository", userTOTPRepository);
    }

    @Test
    public void testGetSecretKey() {
        String username = "testuser";
        UserTOTP userTOTP = new UserTOTP(username, "secretKey", 1234, Arrays.asList(111, 222, 333));
        Map<String, UserTOTP> usersKeys = getUserKeysMap(credentialRepository);
        usersKeys.put(username, userTOTP);

        when(userTOTPRepository.findAllByUsername(username)).thenReturn(Arrays.asList(userTOTP));

        String secretKey = credentialRepository.getSecretKey(username);
        assertEquals("secretKey", secretKey);
    }

    private Map<String, UserTOTP> getUserKeysMap(CredentialRepository credentialRepository) {
        try {
            return (Map<String, UserTOTP>) getField(credentialRepository, "usersKeys");
        } catch (Exception e) {
            throw new RuntimeException("Failed to access the usersKeys field.", e);
        }
    }

    private Object getField(Object targetObject, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field field = targetObject.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.get(targetObject);
    }

    @Test
    public void testSaveUserCredentials() {
        String username = "testuser";
        String secretKey = "secretKey";
        int validationCode = 1234;
        List<Integer> scratchCodes = Arrays.asList(111, 222, 333);

        credentialRepository.saveUserCredentials(username, secretKey, validationCode, scratchCodes);

        verify(userTOTPRepository, times(1)).save(new UserTOTP(username, secretKey, validationCode, scratchCodes));
    }

    @Test
    public void testGetUser() {
        String username = "testuser";
        UserTOTP userTOTP = new UserTOTP(username, "secretKey", 1234, Arrays.asList(111, 222, 333));
        setUserKeysMap(credentialRepository, createUserKeysMap(userTOTP));

        UserTOTP result = credentialRepository.getUser(username);
        assertEquals(userTOTP, result);
    }

    private Map<String, UserTOTP> createUserKeysMap(UserTOTP userTOTP) {
        Map<String, UserTOTP> usersKeys = new HashMap<>();
        usersKeys.put(userTOTP.getUsername(), userTOTP);
        return usersKeys;
    }

    private void setUserKeysMap(CredentialRepository credentialRepository, Map<String, UserTOTP> usersKeys) {
        try {
            setField(credentialRepository, "usersKeys", usersKeys);
        } catch (Exception e) {
            throw new RuntimeException("Failed to set the usersKeys field.", e);
        }
    }

    private void setField(Object targetObject, String fieldName, Object value) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field field = targetObject.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, value);
    }

    @Test
    public void testLoadUserTOTP() {
        String username = "testuser";
        UserTOTP userTOTP = new UserTOTP(username, "secretKey", 1234, Arrays.asList(111, 222, 333));
        List<UserTOTP> userTOTPs = Arrays.asList(userTOTP);

        UserTOTPRepository userTOTPRepository = mock(UserTOTPRepository.class);
        when(userTOTPRepository.findAllByUsername(username)).thenReturn(userTOTPs);

        setUserTOTPRepository(credentialRepository, userTOTPRepository);

        credentialRepository.loadUserTOTP(username);

        Map<String, UserTOTP> usersKeys = getUsersKeys(credentialRepository);
        assertEquals(1, usersKeys.size());
        assertTrue(usersKeys.containsKey(username));
        assertEquals(userTOTP, usersKeys.get(username));

        verify(userTOTPRepository, times(1)).findAllByUsername(username);
    }

    private void setUserTOTPRepository(CredentialRepository credentialRepository, UserTOTPRepository userTOTPRepository) {
        try {
            setField(credentialRepository, "userTOTPRepository", userTOTPRepository);
        } catch (Exception e) {
            throw new RuntimeException("Failed to set the userTOTPRepository field.", e);
        }
    }

    private Map<String, UserTOTP> getUsersKeys(CredentialRepository credentialRepository) {
        try {
            return (Map<String, UserTOTP>) getField(credentialRepository, "usersKeys");
        } catch (Exception e) {
            throw new RuntimeException("Failed to get the usersKeys field.", e);
        }
    }
}