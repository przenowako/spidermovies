package com.spidermovies.spidermovies.security.services;

import com.spidermovies.spidermovies.model.Role;
import com.spidermovies.spidermovies.model.Roles;
import com.spidermovies.spidermovies.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserDetailsImplTest {

    private UserDetailsImpl userDetails;

    @BeforeEach
    public void setup() {
        User user = new User();
        user.setId(1L);
        user.setUsername("testuser");
        user.setEmail("test@example.com");
        user.setPassword("password");
        user.setRole(new Role(1, Roles.USER));

        userDetails = UserDetailsImpl.build(user);
    }

    @Test
    public void testGetAuthorities() {
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        List<String> authorityNames = new ArrayList<>();
        for (GrantedAuthority authority : authorities) {
            authorityNames.add(authority.getAuthority());
        }

        assertEquals(1, authorityNames.size());
        assertTrue(authorityNames.contains("USER"));
    }

    @Test
    public void testGetId() {
        Long id = userDetails.getId();
        assertEquals(1L, id);
    }

    @Test
    public void testGetEmail() {
        String email = userDetails.getEmail();
        assertEquals("test@example.com", email);
    }

    @Test
    public void testGetPassword() {
        String password = userDetails.getPassword();
        assertEquals("password", password);
    }

    @Test
    public void testGetUsername() {
        String username = userDetails.getUsername();
        assertEquals("testuser", username);
    }

    @Test
    public void testIsAccountNonExpired() {
        boolean accountNonExpired = userDetails.isAccountNonExpired();
        assertTrue(accountNonExpired);
    }

    @Test
    public void testIsAccountNonLocked() {
        boolean accountNonLocked = userDetails.isAccountNonLocked();
        assertTrue(accountNonLocked);
    }

    @Test
    public void testIsCredentialsNonExpired() {
        boolean credentialsNonExpired = userDetails.isCredentialsNonExpired();
        assertTrue(credentialsNonExpired);
    }

    @Test
    public void testIsEnabled() {
        boolean enabled = userDetails.isEnabled();
        assertTrue(enabled);
    }

    @Test
    public void testEquals() {
        UserDetailsImpl sameUserDetails = new UserDetailsImpl(1L, "testuser", "test@example.com",
                "password", List.of(new SimpleGrantedAuthority("USER")));

        assertEquals(userDetails, sameUserDetails);
    }

    @Test
    public void testHashCode() {
        UserDetailsImpl sameUserDetails = new UserDetailsImpl(1L, "testuser", "test@example.com",
                "password", List.of(new SimpleGrantedAuthority("USER")));

        assertEquals(userDetails.hashCode(), sameUserDetails.hashCode());
    }
}