package com.spidermovies.spidermovies.security.jwt;

import com.spidermovies.spidermovies.security.services.UserDetailsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseCookie;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import java.lang.reflect.Field;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class JwtUtilsTest {

    private JwtUtils jwtUtils;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        jwtUtils = new JwtUtils();
        ReflectionTestUtils.setField(jwtUtils, "jwtSecret", "testSecretKey");
        ReflectionTestUtils.setField(jwtUtils, "jwtExpirationMs", 10000);
        ReflectionTestUtils.setField(jwtUtils, "jwtCookie", "jwtCookie");
    }

    @Test
    void testGetJwtFromCookies_WithValidCookie_ReturnsJwtToken() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        Cookie cookie = new Cookie("jwtCookie", "testJwtToken");
        request.setCookies(cookie);

        String jwtToken = jwtUtils.getJwtFromCookies(request);

        assertEquals("testJwtToken", jwtToken);
    }

    @Test
    void testGetJwtFromCookies_WithInvalidCookie_ReturnsNull() {
        MockHttpServletRequest request = new MockHttpServletRequest();

        String jwtToken = jwtUtils.getJwtFromCookies(request);

        assertNull(jwtToken);
    }


    @Test
    void testGenerateJwtCookie_ReturnsResponseCookie() throws Exception {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Cookie mockCookie = Mockito.mock(Cookie.class);
        Mockito.when(request.getCookies()).thenReturn(new Cookie[] { mockCookie });
        Mockito.when(mockCookie.getName()).thenReturn("jwtCookie");
        Mockito.when(mockCookie.getValue()).thenReturn("testToken");

        JwtUtils jwtUtils = new JwtUtils();

        Field jwtSecretField = JwtUtils.class.getDeclaredField("jwtSecret");
        jwtSecretField.setAccessible(true);
        jwtSecretField.set(jwtUtils, "testSecretKey");

        jwtUtils.setJwtCookie("jwtCookie");

        UserDetailsImpl userPrincipal = new UserDetailsImpl(
                123L,
                "testUser",
                "password",
                "test@example.com",
                Collections.singletonList(new SimpleGrantedAuthority("USER"))
        );

        ResponseCookie responseCookie = jwtUtils.generateJwtCookie(userPrincipal);

        assertNotNull(responseCookie);
        assertEquals("jwtCookie", responseCookie.getName());
        assertNotNull(responseCookie.getValue());
        assertEquals("/", responseCookie.getPath());
        assertTrue(responseCookie.isHttpOnly());
    }

    @Test
    void testGetCleanJwtCookie_ReturnsResponseCookie() {
        ResponseCookie responseCookie = jwtUtils.getCleanJwtCookie();

        assertNotNull(responseCookie);
        assertEquals("jwtCookie", responseCookie.getName());
        assertEquals("", responseCookie.getValue());
        assertEquals("/", responseCookie.getPath());
    }

    @Test
    void testGetUserNameFromJwtToken_ReturnsUsername() {
        String username = "testUser";
        String token = jwtUtils.generateTokenFromUsername(username);

        String userNameFromJwtToken = jwtUtils.getUserNameFromJwtToken(token);
        assertEquals(username, userNameFromJwtToken);
    }

    @Test
    void testValidateJwtToken_WithValidToken_ReturnsTrue() {
        String username = "testUser";
        String token = jwtUtils.generateTokenFromUsername(username);


        boolean result = jwtUtils.validateJwtToken(token);

        assertTrue(result);
    }

    @Test
    void testValidateJwtToken_WithExpiredToken_ReturnsFalse() {
        String authToken = "expiredJwtToken";

        boolean result = jwtUtils.validateJwtToken(authToken);

        assertFalse(result);}

    @Test
    void testValidateJwtToken_WithInvalidToken_ReturnsFalse() {
        String authToken = "invalidJwtToken";

        boolean result = jwtUtils.validateJwtToken(authToken);

        assertFalse(result);
    }

    @Test
    void testValidateJwtToken_WithUnsupportedToken_ReturnsFalse() {
        String authToken = "unsupportedJwtToken";

        boolean result = jwtUtils.validateJwtToken(authToken);

        assertFalse(result);
    }

    @Test
    void testValidateJwtToken_WithEmptyClaims_ReturnsFalse() {
        String authToken = "emptyClaimsJwtToken";

        boolean result = jwtUtils.validateJwtToken(authToken);

        assertFalse(result);
    }

    @Test
    void testGenerateTokenFromUsername_ReturnsJwtToken() {
        String username = "testUser";

        String jwtToken = jwtUtils.generateTokenFromUsername(username);

        assertNotNull(jwtToken);
    }
}