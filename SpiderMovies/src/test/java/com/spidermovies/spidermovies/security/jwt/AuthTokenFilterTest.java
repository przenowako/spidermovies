package com.spidermovies.spidermovies.security.jwt;

import com.spidermovies.spidermovies.security.services.UserDetailsImpl;
import com.spidermovies.spidermovies.security.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AuthTokenFilterTest {

    private AuthTokenFilter authTokenFilter;
    private JwtUtils jwtUtils;
    private UserDetailsServiceImpl userDetailsService;

    @BeforeEach
    void setUp() {
        authTokenFilter = new AuthTokenFilter();
        jwtUtils = mock(JwtUtils.class);
        userDetailsService = mock(UserDetailsServiceImpl.class);

        try {
            Field jwtUtilsField = AuthTokenFilter.class.getDeclaredField("jwtUtils");
            jwtUtilsField.setAccessible(true);
            jwtUtilsField.set(authTokenFilter, jwtUtils);

            Field userDetailsServiceField = AuthTokenFilter.class.getDeclaredField("userDetailsService");
            userDetailsServiceField.setAccessible(true);
            userDetailsServiceField.set(authTokenFilter, userDetailsService);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testDoFilterInternal_ValidJwtToken_AuthenticationSet() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        UserDetailsImpl userDetails = mock(UserDetailsImpl.class);
        Authentication authentication = mock(Authentication.class);

        String jwtToken = "validJwtToken";
        String username = "testUser";

        when(jwtUtils.validateJwtToken(jwtToken)).thenReturn(true);
        when(jwtUtils.getUserNameFromJwtToken(jwtToken)).thenReturn(username);
        when(userDetails.getUsername()).thenReturn(username);
        when(userDetailsService.loadUserByUsername(username)).thenReturn(userDetails);
        when(authentication.getPrincipal()).thenReturn(userDetails);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        authTokenFilter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(any(), any());
        assertEquals(authentication, SecurityContextHolder.getContext().getAuthentication());
    }
}