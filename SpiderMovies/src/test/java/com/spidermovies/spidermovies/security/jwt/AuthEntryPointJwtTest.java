package com.spidermovies.spidermovies.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.AuthenticationException;

import java.io.IOException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class AuthEntryPointJwtTest {

    @Test
    void testCommence_ReturnsUnauthorizedResponse() throws IOException {
        AuthenticationException authException = mock(AuthenticationException.class);
        Mockito.when(authException.getMessage()).thenReturn("Unauthorized");

        AuthEntryPointJwt entryPoint = new AuthEntryPointJwt();

        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        entryPoint.commence(request, response, authException);

        assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatus());
        assertEquals(MediaType.APPLICATION_JSON_VALUE, response.getContentType());

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseBody = mapper.readValue(response.getContentAsString(), Map.class);

        assertEquals(HttpStatus.UNAUTHORIZED.value(), responseBody.get("status"));
        assertEquals("Unauthorized", responseBody.get("error"));
        assertEquals("Unauthorized", responseBody.get("message"));
        assertEquals(request.getServletPath(), responseBody.get("path"));
    }
}