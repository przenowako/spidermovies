package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.UserTOTP;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserTOTPRepositoryTest {

    @Autowired
    private UserTOTPRepository userTOTPRepository;

    @Test
    public void userTOTPRepositoryTest() {
        UserTOTP userTOTP = new UserTOTP();
        userTOTP.setUsername("testUsername");
        userTOTP.setSecretKey("testSecretKey");

        userTOTP = userTOTPRepository.saveAndFlush(userTOTP);
        Optional<UserTOTP> optionalUserTOTP = userTOTPRepository.findById(userTOTP.getUsername());
        assertTrue(optionalUserTOTP.isPresent());
        assertEquals(userTOTP.getUsername(), optionalUserTOTP.get().getUsername());

        List<UserTOTP> userTOTPList = userTOTPRepository.findAllByUsername(userTOTP.getUsername());
        assertFalse(userTOTPList.isEmpty());
        assertEquals(userTOTP.getUsername(), userTOTPList.get(0).getUsername());

        userTOTPRepository.delete(userTOTP);
        Optional<UserTOTP> optionalUserTOTP1 = userTOTPRepository.findById(userTOTP.getUsername());
        assertTrue(optionalUserTOTP1.isEmpty());
    }
}
