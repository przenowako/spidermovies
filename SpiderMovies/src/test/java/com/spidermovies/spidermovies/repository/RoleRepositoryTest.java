package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.Role;
import com.spidermovies.spidermovies.model.Roles;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void roleRepositoryTest() {
        Role role = new Role();
        role.setName(Roles.ADMIN);

        role = roleRepository.saveAndFlush(role);
        Optional<Role> optionalRole = roleRepository.findById(role.getId());
        assertTrue(optionalRole.isPresent());
        assertEquals(role.getId(),optionalRole.get().getId());
        assertTrue(roleRepository.existsById(role.getId()));

        role.setName(Roles.MANAGER);
        role =  roleRepository.saveAndFlush(role);
        optionalRole = roleRepository.findById(role.getId());
        assertTrue(optionalRole.isPresent());
        assertEquals(optionalRole.get().getName(), role.getName());

        roleRepository.delete(role);
        Optional<Role> optionalRole1 = roleRepository.findById(role.getId());
        assertTrue(optionalRole1.isEmpty());
    }
}
