package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.Role;
import com.spidermovies.spidermovies.model.Roles;
import com.spidermovies.spidermovies.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Test
    public void userRepositoryTest() {
        Role testRole = new Role();
        roleRepository.saveAndFlush(testRole);
        testRole.setName(Roles.USER);

        User user = new User("testUserName", "testEmail@test.test", "password");
        user.setName("name");
        user.setLastName("lastName");
        user.setAddress("address");
        user.setRole(testRole);

        user = userRepository.saveAndFlush(user);
        Optional<User> optionalUser = userRepository.findUserById(user.getId());
        assertTrue(optionalUser.isPresent());
        assertEquals(user.getId(), optionalUser.get().getId());

        optionalUser = userRepository.findByUsername(user.getUsername());
        assertTrue(optionalUser.isPresent());
        assertEquals(user.getId(), optionalUser.get().getId());
        assertTrue(userRepository.existsByUsername(user.getUsername()));
        assertTrue(userRepository.existsByEmail(user.getEmail()));
        assertTrue(userRepository.existsById(user.getId()));

        user.setName("Name2");
        user = userRepository.saveAndFlush(user);
        optionalUser = userRepository.findUserById(user.getId());
        assertTrue(optionalUser.isPresent());
        assertEquals(optionalUser.get().getName(), user.getName());

        userRepository.delete(user);
        Optional<User> optionalUser1 = userRepository.findById(user.getId());
        assertTrue(optionalUser1.isEmpty());

        roleRepository.delete(testRole);
    }
}
