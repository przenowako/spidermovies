package com.spidermovies.spidermovies.repository;

import com.spidermovies.spidermovies.model.Movie;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class MovieRepositoryTest {

    @Autowired
    private MovieRepository movieRepository;


    @Test
    public void movieRepositoryTest() {
        Date date = new Date();
        Movie movie = new Movie();
        movie.setTitle("testTitle");
        movie.setAuthor("testAuthor");
        movie.setImage("www.testImage.pl");
        movie.setPrice(99.99F);
        movie.setReleaseDate(date);
        movie.setAgeClassification(18);
        movie.setDescription("testDescription");
        movie.setGenres(null);

        movie = movieRepository.saveAndFlush(movie);
        Optional<Movie> optionalMovie = movieRepository.findMovieById(movie.getId());
        assertTrue(optionalMovie.isPresent());
        assertEquals(movie.getId(), optionalMovie.get().getId());
        assertTrue(movieRepository.existsById(movie.getId()));

        Page<Movie> moviePage = movieRepository
                .findAllByTitleContainingOrAuthorContaining(movie.getTitle(),null, Pageable.unpaged());
        assertTrue(moviePage.hasContent());

        moviePage = movieRepository
                .findAllByTitleContainingOrAuthorContaining(null, movie.getAuthor(), Pageable.unpaged());
        assertTrue(moviePage.hasContent());

        Movie finalMovie = movie;
        assertTrue(moviePage.getContent().stream()
                .filter(x -> Objects.equals(x.getId(), finalMovie.getId()))
                .count() > 0);

        movie.setTitle("testTitle2");
        movie.setAuthor("testAuthor2");
        movie.setImage("www.testImage2.pl");
        movie.setPrice(15.99F);
        movie.setAgeClassification(0);
        movie.setDescription("testDescription2");
        movie =  movieRepository.saveAndFlush(movie);
        assertEquals(movie.getTitle(),"testTitle2");
        assertEquals(movie.getAuthor(),"testAuthor2");
        assertEquals(movie.getImage(),"www.testImage2.pl");
        assertEquals(movie.getDescription(),"testDescription2");
        assertEquals(15.99F, movie.getPrice());

        movieRepository.delete(movie);
        Optional<Movie> optionalMovie1 = movieRepository.findById(movie.getId());
        assertTrue(optionalMovie1.isEmpty());
    }
}

