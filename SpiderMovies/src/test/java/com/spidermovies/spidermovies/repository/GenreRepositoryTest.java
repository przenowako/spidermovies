package com.spidermovies.spidermovies.repository;
import com.spidermovies.spidermovies.model.Genre;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class GenreRepositoryTest {

    @Autowired
    GenreRepository genreRepository;

    @Test
    public void genreRepositoryTest() {
        Genre genre = new Genre();
        genre.setName("Genre1");

        genre = genreRepository.saveAndFlush(genre);
        Optional<Genre> optionalGenre = genreRepository.findGenreById(genre.getId());
        assertTrue(optionalGenre.isPresent());
        assertEquals(genre.getId(), optionalGenre.get().getId());

        List<Long> longList = List.of(genre.getId());
        Set<Genre> genreSet = genreRepository.findByIdIn(longList);
        assertThat(genreSet.contains(genre));

        genre.setName("Genre2");
        genre =  genreRepository.saveAndFlush(genre);
        optionalGenre = genreRepository.findById(genre.getId());
        assertTrue(optionalGenre.isPresent());
        assertEquals(genre.getName(), optionalGenre.get().getName());

        genreRepository.delete(genre);
        Optional<Genre> stOpt = genreRepository.findById(genre.getId());
        assertFalse(stOpt.isPresent());
    }
}

