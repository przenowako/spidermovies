package com.spidermovies.spidermovies.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GenreNotFoundExceptionTest {

    @Test
    void testGenreNotFoundException_GetMessage_ReturnsErrorMessage() {
        String errorMessage = "Genre not found";
        GenreNotFoundException exception = new GenreNotFoundException(errorMessage);

        assertEquals(errorMessage, exception.getMessage());
    }
}