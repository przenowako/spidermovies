package com.spidermovies.spidermovies.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieNotFoundExceptionTest {

    @Test
    void testMovieNotFoundException_GetMessage_ReturnsErrorMessage() {
        String errorMessage = "Movie not found";
        MovieNotFoundException exception = new MovieNotFoundException(errorMessage);

        assertEquals(errorMessage, exception.getMessage());
    }
}