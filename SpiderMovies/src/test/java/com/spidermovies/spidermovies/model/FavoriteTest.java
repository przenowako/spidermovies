package com.spidermovies.spidermovies.model;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FavoriteTest {

    @Test
    public void testFavoriteValidation() {
        Favorite favorite = new Favorite();
        favorite.setUserId(1L);
        favorite.setMovieId(null);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Favorite>> violations = validator.validate(favorite);

        assertEquals(1, violations.size());

        ConstraintViolation<Favorite> violation = violations.iterator().next();
        assertEquals("nie może mieć wartości null", violation.getMessage());
        assertEquals("movieId", violation.getPropertyPath().toString());

        favorite.setUserId(null);
        favorite.setMovieId(1L);

        violations = validator.validate(favorite);

        assertEquals(1, violations.size());

        violation = violations.iterator().next();
        assertEquals("nie może mieć wartości null", violation.getMessage());
        assertEquals("userId", violation.getPropertyPath().toString());
    }
}