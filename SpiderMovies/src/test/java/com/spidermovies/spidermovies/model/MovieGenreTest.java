package com.spidermovies.spidermovies.model;

import static org.junit.jupiter.api.Assertions.*;

import com.spidermovies.spidermovies.model.MovieGenre;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieGenreTest {

    @Test
    public void testMovieGenreValidation() {
        MovieGenre movieGenre = new MovieGenre();
        movieGenre.setMovieId(1L);
        movieGenre.setGenreId(null);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<MovieGenre>> violations = validator.validate(movieGenre);

        assertEquals(1, violations.size());

        ConstraintViolation<MovieGenre> violation = violations.iterator().next();
        assertEquals("nie może mieć wartości null", violation.getMessage());
        assertEquals("genreId", violation.getPropertyPath().toString());

        movieGenre.setMovieId(null);
        movieGenre.setGenreId(1L);

         violations = validator.validate(movieGenre);

        assertEquals(1, violations.size());

        violation = violations.iterator().next();
        assertEquals("nie może mieć wartości null", violation.getMessage());
        assertEquals("movieId", violation.getPropertyPath().toString());
    }

}