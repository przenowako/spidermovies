package com.spidermovies.spidermovies.model;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserTest {

    @Test
    public void testUserValidation() {
        User user = new User();
        user.setUsername("");
        user.setEmail("invalid_email");
        user.setPassword("");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        assertEquals(5, violations.size());

        for (ConstraintViolation<User> violation : violations) {
            String errorMessage = violation.getMessage();
            assertTrue(errorMessage.equals("nie może być odstępem") ||
                    errorMessage.equals("musi być poprawnie sformatowanym adresem e-mail") ||
                    errorMessage.equals("wielkość musi należeć do zakresu od 3 do 50") ||
                    errorMessage.equals("wielkość musi należeć do zakresu od 6 do 120"));
        }
    }
}