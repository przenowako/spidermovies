package com.spidermovies.spidermovies.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class ConstantsTest {

    @Test
    public void testMaxName() {
        assertEquals(120, Constants.MAX_NAME);
    }

    @Test
    public void testMaxDescription() {
        assertEquals(1000, Constants.MAX_DESCRIPTION);
    }

    @Test
    public void testDataPattern() {
        assertEquals("yyyy-MM-dd", Constants.DATA_PATTERN);
    }

    @Test
    public void testMaxRoleName() {
        assertEquals(20, Constants.MAX_ROLE_NAME);
    }

    @Test
    public void testMaxPassword() {
        assertEquals(120, Constants.MAX_PASSWORD);
    }

    @Test
    public void testMinPassword() {
        assertEquals(6, Constants.MIN_PASSWORD);
    }

    @Test
    public void testMaxEmail() {
        assertEquals(50, Constants.MAX_EMAIL);
    }

    @Test
    public void testMinEmail() {
        assertEquals(5, Constants.MIN_EMAIL);
    }

    @Test
    public void testMaxUsername() {
        assertEquals(50, Constants.MAX_USERNAME);
    }

    @Test
    public void testMinUsername() {
        assertEquals(3, Constants.MIN_USERNAME);
    }

    @Test
    public void testMaxAgeSeconds() {
        assertEquals(86400, Constants.MAX_AGE_SECONDS);
    }

    @Test
    public void testMaxAge() {
        assertEquals(3600, Constants.MAX_AGE);
    }

    @Test
    public void testQRCodeSize() {
        assertEquals(200, Constants.QR_CODE_SIZE);
    }

    @Test
    public void testPrivateConstructor() {
        try {
            Constructor<Constants> constructor = Constants.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            constructor.newInstance();
            fail("Expected exception not thrown");
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            Assertions.assertEquals(AssertionError.class, e.getCause().getClass());
            assertEquals("Constants class should not be instantiated", e.getCause().getMessage());
        }
    }

}