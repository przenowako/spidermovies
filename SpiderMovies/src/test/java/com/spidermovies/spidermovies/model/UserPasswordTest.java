package com.spidermovies.spidermovies.model;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserPasswordTest {

    @Test
    public void testNoArgsConstructor() {
        UserPassword userPassword = new UserPassword();

        Long userId = 123L;
        String currentPassword = "currentPassword";
        String newPassword = "newPassword";

        userPassword.setUserId(userId);
        userPassword.setCurrentPassword(currentPassword);
        userPassword.setNewPassword(newPassword);

        assertEquals(userId, userPassword.getUserId());
        assertEquals(currentPassword, userPassword.getCurrentPassword());
        assertEquals(newPassword, userPassword.getNewPassword());
    }

    @Test
    public void testAllArgsConstructor() {
        Long userId = 123L;
        String currentPassword = "currentPassword";
        String newPassword = "newPassword";

        UserPassword userPassword = new UserPassword(userId, currentPassword, newPassword);

        assertEquals(userId, userPassword.getUserId());
        assertEquals(currentPassword, userPassword.getCurrentPassword());
        assertEquals(newPassword, userPassword.getNewPassword());
    }

    @Test
    public void testGettersAndSetters() {
        Long id = 1L;
        String currentPassword = "CurrentPassword1@";
        String newPassword = "NewPassword@1";

        UserPassword userPassword  = new UserPassword(id, currentPassword, newPassword);

        assertEquals(id, userPassword.getUserId());
        assertEquals(currentPassword, userPassword.getCurrentPassword());
        assertEquals(newPassword, userPassword.getNewPassword());
    }

    @Test
    public void testUserPasswordValidation() {
        UserPassword userPassword = new UserPassword();
        userPassword.setUserId(null);
        userPassword.setCurrentPassword("");
        userPassword.setNewPassword("");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<UserPassword>> violations = validator.validate(userPassword);

        assertEquals(5, violations.size());

        for (ConstraintViolation<UserPassword> violation : violations) {
            String errorMessage = violation.getMessage();
            assertTrue(errorMessage.equals("nie może mieć wartości null") ||
                    errorMessage.equals("wielkość musi należeć do zakresu od 6 do 120") ||
                    errorMessage.equals("nie może być odstępem"));
        }
    }
}