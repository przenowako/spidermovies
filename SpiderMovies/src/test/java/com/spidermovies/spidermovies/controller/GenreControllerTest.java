package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.Genre;
import com.spidermovies.spidermovies.repository.GenreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class GenreControllerTest {

    @Mock
    private GenreRepository genreRepository;

    @InjectMocks
    private GenreController genreController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllGenres() {
        List<Genre> genres = new ArrayList<>();
        genres.add(new Genre(1L, "Action"));
        genres.add(new Genre(2L, "Comedy"));

        when(genreRepository.findAll()).thenReturn(genres);

        ResponseEntity<List<Genre>> response = genreController.getAllGenres();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(genres, response.getBody());
        verify(genreRepository, times(1)).findAll();
    }

    @Test
    public void testGetGenreById() {
        long genreId = 1L;
        Genre genre = new Genre(genreId, "Action");

        when(genreRepository.findGenreById(genreId)).thenReturn(Optional.of(genre));

        ResponseEntity<Genre> response = genreController.getGenreById(genreId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(genre, response.getBody());
        verify(genreRepository, times(1)).findGenreById(genreId);
    }

    @Test
    public void testGetGenreById_GenreNotFound() {
        long genreId = 1L;

        when(genreRepository.findGenreById(genreId)).thenReturn(Optional.empty());

        ResponseEntity<Genre> response = genreController.getGenreById(genreId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertTrue(response.getBody() == null || response.getBody().equals(Optional.empty()));
        verify(genreRepository, times(1)).findGenreById(genreId);
    }

    @Test
    public void testCreateGenre() {
        String genreName = "Action";
        Genre newGenre = new Genre();
        newGenre.setName(genreName);

        when(genreRepository.save(any(Genre.class))).thenReturn(newGenre);

        ResponseEntity<Genre> response = genreController.createGenre(genreName);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(newGenre.getName(), response.getBody().getName());
        verify(genreRepository, times(1)).save(any(Genre.class));
    }

    @Test
    public void testUpdateGenre() {
        long genreId = 1L;
        Genre existingGenre = new Genre(genreId, "Action");
        Genre updatedGenre = new Genre(genreId, "Comedy");

        when(genreRepository.findGenreById(genreId)).thenReturn(Optional.of(existingGenre));
        when(genreRepository.save(any(Genre.class))).thenReturn(updatedGenre);

        ResponseEntity<Genre> response = genreController.updateGenre(genreId, updatedGenre);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedGenre, response.getBody());
        verify(genreRepository, times(1)).findGenreById(genreId);
        verify(genreRepository, times(1)).save(any(Genre.class));
    }

    @Test
    public void testUpdateGenre_GenreNotFound() {
        long genreId = 1L;
        Genre updatedGenre = new Genre(genreId, "Comedy");

        when(genreRepository.findGenreById(genreId)).thenReturn(Optional.empty());

        ResponseEntity<Genre> response = genreController.updateGenre(genreId, updatedGenre);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertTrue(response.getBody() == null || response.getBody().equals(Optional.empty()));
        verify(genreRepository, times(1)).findGenreById(genreId);
        verify(genreRepository, never()).save(any(Genre.class));
    }

    @Test
    public void testDeleteGenre() {
        long genreId = 1L;

        ResponseEntity<HttpStatus> response = genreController.deleteGenre(genreId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(genreRepository, times(1)).deleteById(genreId);
    }

    @Test
    public void testDeleteGenre_GenreNotFound() {
        long genreId = 1L;

        when(genreRepository.findById(genreId)).thenReturn(Optional.empty());

        ResponseEntity<HttpStatus> response = genreController.deleteGenre(genreId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(genreRepository, times(1)).findById(genreId);
    }


    @Test
    void testDeleteGenre_ExceptionHandling() {
        long genreId = 1L;
        when(genreRepository.findById(genreId)).thenReturn(Optional.empty());

        doThrow(RuntimeException.class).when(genreRepository).deleteById(genreId);

        ResponseEntity<HttpStatus> response = genreController.deleteGenre(genreId);

        verify(genreRepository, times(1)).findById(genreId);
        verify(genreRepository, times(1)).deleteById(genreId);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}