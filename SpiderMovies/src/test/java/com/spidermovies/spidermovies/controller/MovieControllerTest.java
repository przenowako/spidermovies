package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.Movie;
import com.spidermovies.spidermovies.repository.GenreRepository;
import com.spidermovies.spidermovies.repository.MovieRepository;
import com.spidermovies.spidermovies.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MovieControllerTest {

    @Mock
    private MovieRepository movieRepository;

    @Mock
    private GenreRepository genreRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private MovieController movieController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateMovie_ExceptionHandling() {
        Movie movie = new Movie();
        doThrow(RuntimeException.class).when(movieRepository).save(movie);

        ResponseEntity<Movie> response = movieController.createMovie(movie);

        verify(movieRepository, times(1)).save(movie);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(null, response.getBody());
    }

    @Test
    public void testUpdateMovie_MovieNotFound() {
        long movieId = 1L;
        Movie updatedMovie = new Movie();
        when(movieRepository.findById(movieId)).thenReturn(Optional.empty());

        ResponseEntity<Movie> response = movieController.updateMovie(movieId, updatedMovie);

        verify(movieRepository, times(1)).findById(movieId);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
    }

    @Test
    public void testDeleteMovie_ExceptionHandling() {
        long movieId = 1L;
        when(movieRepository.findById(movieId)).thenReturn(Optional.empty());
        doThrow(RuntimeException.class).when(movieRepository).deleteById(movieId);

        ResponseEntity<HttpStatus> response = movieController.deleteMovie(movieId);

        verify(movieRepository, times(1)).findById(movieId);
        verify(movieRepository, times(1)).deleteById(movieId);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    public void testGetMovieDetails_MovieDataNotPresent() {
        long movieId = 1L;
        long userId = 1L;
        when(movieRepository.findById(movieId)).thenReturn(Optional.empty());
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        ResponseEntity<Movie> response = movieController.getMovieById(movieId, userId);

        verify(movieRepository, times(1)).findById(movieId);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
    }
}
