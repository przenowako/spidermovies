package com.spidermovies.spidermovies.controller;

import com.spidermovies.spidermovies.model.Favorite;
import com.spidermovies.spidermovies.model.Genre;
import com.spidermovies.spidermovies.model.User;
import com.spidermovies.spidermovies.model.Movie;
import com.spidermovies.spidermovies.repository.MovieRepository;
import com.spidermovies.spidermovies.repository.UserRepository;
import com.spidermovies.spidermovies.repository.UserTOTPRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private MovieRepository movieRepository;

    @Mock
    private UserTOTPRepository userTOTPRepository;

    @Mock
    private PasswordEncoder encoder;

    @InjectMocks
    private UserController userController;

    private User testUser;
    private Movie testMovie;

    @BeforeEach
    public void setup() {
        testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("testuser");
        testUser.setEmail("testuser@example.com");

        testMovie = new Movie();
        testMovie.setId(1L);
        testMovie.setTitle("Test Movie");
        Set<Genre> genreSet = new HashSet<>();
        genreSet.add(new Genre(1L, "Action"));
        testMovie.setGenres(genreSet);
    }

    @Test
    public void testGetAllUsersPage() {
        List<User> users = Collections.singletonList(testUser);
        PageImpl<User> userPage = new PageImpl<>(users);
        Sort.Order sort = new Sort.Order(Sort.Direction.ASC, "id");
        when(userRepository.findAll(any(PageRequest.class))).thenReturn(userPage);

        ResponseEntity<Map<String, Object>> response = userController.getAllUsersPage(null, null, 0, 3, new String[]{"id,asc"});

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().containsKey("users"));
        assertTrue(response.getBody().containsKey("currentPage"));
        assertTrue(response.getBody().containsKey("totalItems"));
        assertTrue(response.getBody().containsKey("totalPages"));
        assertEquals(users, response.getBody().get("users"));
        assertEquals(0, response.getBody().get("currentPage"));
        assertEquals(1L, response.getBody().get("totalItems"));
        assertEquals(1, response.getBody().get("totalPages"));

        verify(userRepository, times(1)).findAll(any(PageRequest.class));
    }

    @Test
    public void testGetUserById() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(testUser));

        ResponseEntity<User> response = userController.getUserById(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(testUser, response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test
    public void testGetUserById_UserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        ResponseEntity<User> response = userController.getUserById(1L);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test
    public void testUpdateUser() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(testUser));

        testUser.setUsername("newusername");
        testUser.setEmail("newmail@example.com");

        ResponseEntity<User> response = userController.updateUser(testUser.getId(), testUser);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(userRepository, times(1)).findById(anyLong());
        verify(userRepository, times(1)).save(any(User.class));
    }


    @Test
    public void testUpdateUser_UserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        User updatedUser = new User();
        updatedUser.setUsername("newusername");
        updatedUser.setEmail("newemail@example.com");

        ResponseEntity<User> response = userController.updateUser(1L, updatedUser);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void testDeleteUser() {
        ResponseEntity<?> response = userController.deleteUser(testUser.getId());

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void testDeleteFavoriteMovie_UserOrMovieNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(movieRepository.findMovieById(anyLong())).thenReturn(Optional.empty());

        Favorite favorite = new Favorite();
        favorite.setUserId(1L);
        favorite.setMovieId(1L);

        ResponseEntity<?> response = userController.deleteFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
        verify(movieRepository, times(1)).findMovieById(anyLong());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void testAddFavorite() {
        Set<Movie> movieSet = new HashSet<>();
        movieSet.add(testMovie);
        Favorite favorite = new Favorite(testUser.getId(), testMovie.getId(), movieSet);
        ResponseEntity<?> response = userController.setFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test
    public void testAddFavorite_UserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        Set<Movie> movieSet = new HashSet<>();
        movieSet.add(new Movie());
        Favorite favorite = new Favorite(1L,1L,movieSet);

        ResponseEntity<?> response = userController.setFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
        verify(movieRepository, never()).findById(anyLong());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void testAddFavorite_MovieNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(testUser));

        Set<Movie> movieSet = new HashSet<>();
        movieSet.add(new Movie());
        Favorite favorite = new Favorite(1L,1L,movieSet);

        ResponseEntity<?> response = userController.setFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void testRemoveFavorite() {
        Set<Movie> movieSet = new HashSet<>();
        movieSet.add(new Movie());
        Favorite favorite = new Favorite(1L, 1L, movieSet);

        testUser.setFavorites(movieSet);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(testUser));

        ResponseEntity<?> response = userController.deleteFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test
    public void testRemoveFavorite_UserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        Set<Movie> movieSet = new HashSet<>();
        movieSet.add(new Movie());
        Favorite favorite = new Favorite(1L,1L,movieSet);
        ResponseEntity<?> response = userController.deleteFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
        verify(movieRepository, never()).findById(anyLong());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void testRemoveFavorite_MovieNotFound() {
        Favorite favorite = new Favorite();
        favorite.setUserId(1L);
        favorite.setMovieId(1L);

        ResponseEntity<?> response = userController.deleteFavoriteMovie(favorite);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());

        verify(userRepository, times(1)).findById(anyLong());
        verify(movieRepository, times(1)).findMovieById(anyLong());
        verify(userRepository, never()).save(any(User.class));
    }
}